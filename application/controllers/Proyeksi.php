<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyeksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function tampiljabatan()
	{
		$data = $this->db->get('jabatan')->result();
		echo json_encode($data);
	}
	public function index()
	{
		$data['tahun'] = $this->input->post('tahun');
		if ($data['tahun']==0)
		{
			$data['tahun'] = date('Y');
		}
		$data['title1'] = 'PROYEKSI';
		$data['hal1'] = 'Pejabat Fungsional Proyeksi';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pejabat Fungsional Proyeksi</li>';
		$data['instansi'] = $this->db->get('instansi')->result();
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('proyeksi pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$this->db->where('tahun',$data['tahun']);
		$data['pejafung'] = $this->db->get()->result();
		$data['page'] = 'proyeksi/proyeksi';
		$this->load->view('dashboard',$data);
	}

	public function halaman($tahun)
	{
		$data['tahun'] = $tahun;
		$data['title1'] = 'PROYEKSI';
		$data['hal1'] = 'Pejabat Fungsional Proyeksi';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pejabat Fungsional Proyeksi</li>';
		$data['instansi'] = $this->db->get('instansi')->result();
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('proyeksi pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$this->db->where('tahun',$data['tahun']);
		$data['pejafung'] = $this->db->get()->result();
		$data['page'] = 'proyeksi/proyeksi';
		$this->load->view('dashboard',$data);
	}
	public function edit_pejafung($id,$kk,$tahun)
	{
		$data['id'] = $id;
		$data['kk'] = $kk;
		$data['tahun'] = $tahun;
		$data['edit_pj'] = $this->db->get_where('proyeksi',array('id_pejafung'=>$id))->row();
		// print_r($data['edit_pj']);die();
		$this->load->view('proyeksi/edit_proyeksi_',$data);
	}
	public function update_pejafung()
	{
		$id=$this->input->post('id');
		$tahun=$this->input->post('tahun');
		$id_ = $this->input->post('id_');
		$isi = $this->input->post('isi');
		$pejafung = $this->input->post('pejafung');
		$data2 = array
		(
			$id_ => $isi,
		);
		$this->db->update('proyeksi',$data2,array('id_pejafung'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data Pejafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Proyeksi/halaman/'.$tahun);
	}
}
?>