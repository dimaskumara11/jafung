<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pejafung extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function tampiljabatan()
	{
		$data = $this->db->get('jabatan')->result();
		echo json_encode($data);
	}
	public function index()
	{
		$data['tahun'] = $this->input->post('tahun');
		if ($data['tahun']==0)
		{
			$data['tahun'] = date('Y');
		}
		$data['title1'] = 'Aktif';
		$data['hal1'] = 'Pejabat Fungsional Aktif';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pejabat Fungsional Aktif</li>';
		$data['instansi'] = $this->db->get('instansi')->result();
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('pejafung pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$this->db->where('pj.tahun',$data['tahun']);
		$data['pejafung'] = $this->db->get()->result();
		// print_r($data['pejafung']);die();
		$data['page'] = 'pejafung_aktif/pejafung_aktif';
		$this->load->view('dashboard',$data);
	}

	public function halaman($tahun)
	{
		$data['tahun'] = $tahun;
		$data['title1'] = 'Aktif';
		$data['hal1'] = 'Pejabat Fungsional Aktif';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pejabat Fungsional Aktif</li>';
		$data['instansi'] = $this->db->get('instansi')->result();
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('pejafung pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$this->db->where('tahun',$data['tahun']);
		$data['pejafung'] = $this->db->get()->result();
		$data['page'] = 'pejafung_aktif/pejafung_aktif';
		$this->load->view('dashboard',$data);
	}
	public function edit_pejafung($id,$kk,$pj,$tahun)
	{
		$data['pja'] = $pj;
		$data['id'] = $id;
		$data['kk'] = $kk;
		$data['tahun'] = $tahun;
		$data['edit_pj'] = $this->db->get_where('pejafung',array('id_pejafung'=>$id))->row();
		$this->load->view('pejafung_aktif/edit_pejafung',$data);
	}
	public function update_pejafung()
	{
		$id=$this->input->post('id');
		$tahun=$this->input->post('tahun');
		$pus1 = $this->input->post('pusat_');
		$pus2 = $this->input->post('pusat');
		$pro1 = $this->input->post('provinsi_');
		$pro2 = $this->input->post('provinsi');
		$kab1 = $this->input->post('kabupaten_');
		$kab2 = $this->input->post('kabupaten');
		$pejafung = $this->input->post('pejafung');
		$data1[0] = array(
			'pusatatas50' => $pus1,
			'pusatbawah50' => $pus2,
			'provatas50' => $pro1,
			'provbawah50' => $pro2,
			'kabatas50' => $kab1,
			'kabbawah50' => $kab2,
		);
		$a = json_encode($data1);
		$data2 = array(
		$this->input->post('kk') => $a,
		);
		if ($pejafung == "PROYEKSI")
		{
			$this->db->update('proyeksi',$data2,array('id_pejafung'=>$id));
		}
		else
		{
			$this->db->update('pejafung',$data2,array('id_pejafung'=>$id));
		}
		$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data Pejafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Pejafung/halaman/'.$tahun);
	}
}
?>