<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$hak = $this->session->userdata('hak_akses');
		$data['hal1'] = 'User';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">User</li>';
		$data['title'] = "";
		$data['page'] = "registrasi/registrasi_user";
		$data['user'] = $this->db->get('user')->result();
		$data['instansi'] = $this->db->get('instansi')->result();
		$this->load->view('dashboard',$data);
	}
	public function tampil_user()
	{
		$hak = $this->session->userdata('hak_akses');
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('instansi ins','us.nama_kementrian = ins.id_instansi');
		$data = $this->db->get()->result();
		echo json_encode($data);
	}

	public function tambah_user()
	{
		$hak = $this->session->userdata('hak_akses');
		$nm_us = $this->input->post('nama_user');
		$nm_km = $this->input->post('nama_kementrian');
		$hak = $this->input->post('hak_akses');
		$nik = $this->input->post('nik');
		$email = $this->input->post('email');
		$nohp = $this->input->post('no_hp');
		$pass = md5($this->input->post('password'));
		$tambah_user = $this->db->insert('user',array('nama_user'=>$nm_us,'nama_kementrian'=>$nm_km,'hak_akses'=>$hak,'nik'=>$nik,'password'=>$pass,'email'=>$email,'no_hp'=>$nohp));
		echo json_encode($nm_us);
	}

	public function edit_user($id)
	{
		$hak = $this->session->userdata('hak_akses');
		$data = $this->db->query('select * from user where id_user = '.$id.'')->row_array();
		echo json_encode($data);
	}

	public function delete_user($id)
	{
		$hak = $this->session->userdata('hak_akses');
		$this->db->delete('user',array('id_user' => $id));
		$this->session->set_flashdata('alert','<script>swal("Berhasil Menghapus Data User!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('User');
	}
	public function update_user()
	{
		$hak = $this->session->userdata('hak_akses');
		$id = $this->input->post('id_user');
		$nm_us = $this->input->post('nama_user');
		$nm_km = $this->input->post('nama_kementrian');
		$hak = $this->input->post('hak_akses');
		$nik = $this->input->post('nik');
		$email = $this->input->post('email');
		$nohp = $this->input->post('no_hp');
		$this->db->update('user',array('nama_user'=>$nm_us,'nama_kementrian'=>$nm_km,'hak_akses'=>$hak,'nik'=>$nik,'email'=>$email,'no_hp'=>$nohp),array('id_user'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data User!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('User');
	}
}
?>