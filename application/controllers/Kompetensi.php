<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kompetensi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$tahun_mulai = $this->input->post('tahun_mulai');
		$tahun_sampai = $this->input->post('tahun_sampai');
		$selisih = $tahun_sampai-$tahun_mulai;
		if ($tahun_mulai == 0)
		{
			$data = array(
				'tahun' => date('Y'),
				'no1' => date('Y'),
				'no2' => '2',
				'no3' => '1',
				'no4' => date('Y'),
				'hal1' => 'Pejabat Fungsional Pengembangan Kompetensi',
				'hal2' => '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pengembangan Kompetensi</li>',
				'pengembangan' => $this->db->query('SELECT pk.*,jb.nama_jabatan,ins.nama_instansi from pengembangan_kompetensi pk join jabatan jb on pk.id_jabatan=jb.id_jabatan join instansi ins on pk.id_instansi=ins.id_instansi group by pk.id_jabatan')->result(),
				'instansi' => $this->db->get('instansi')->result(),
				'jabatan' => $this->db->get('jabatan')->result(),
				'page' => 'kompetensi/pengembangan_kompetensi',
			);
		}
		else
		{
			$data = array(
				'tahun' => $tahun_mulai.'-'.$tahun_sampai,
				'no1' => $tahun_mulai,
				'no2' => ($selisih+1)*2,
				'no3' => $selisih+1,
				'no4' => $tahun_sampai,
				'hal1' => 'Pejabat Fungsional Pengembangan Kompetensi',
				'hal2' => '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pengembangan Kompetensi</li>',
				'pengembangan' => $this->db->query('SELECT pk.*,jb.nama_jabatan,ins.nama_instansi from pengembangan_kompetensi pk join jabatan jb on pk.id_jabatan=jb.id_jabatan join instansi ins on pk.id_instansi=ins.id_instansi group by pk.id_jabatan')->result(),
				'instansi' => $this->db->get('instansi')->result(),
				'jabatan' => $this->db->get('jabatan')->result(),
				'page' => 'kompetensi/pengembangan_kompetensi',
			);
		}
		$this->load->view('dashboard',$data);
	}
	public function edit_pejafung_pk($id,$kk,$ks,$ks1,$th)
	{
		$data['id'] = $id;
		$data['kk'] = $kk;
		$data['ks'] = $ks;
		$data['ks1'] = $ks1;
		$data['th'] = $th;
		$data['jabatan'] = $this->input->post('jb');
		$data['instansi'] = $this->input->post('ins');
		$data['smp'] = $this->input->post('smp');
		$data['mul'] = $this->input->post('mul');
		// print_r($th);die();
		$data['edit_pj'] = $this->db->get_where('pengembangan_kompetensi',array('id_jabatan'=>$data['jabatan'],'id_instansi'=>$data['instansi'],'tahun'=>$data['th']))->row();
		$this->load->view('kompetensi/edit_pejafung_pk',$data);
	}
	public function update_pejafung_pk()
	{
		$id=$this->input->post('id');
		$jabatan=$this->input->post('jabatan');
		$instansi=$this->input->post('instansi');
		$kk = $this->input->post('kk');
		$jp = $this->input->post('jumlah_pj');
		$th = $this->input->post('tahun');
		$jp1 = $this->input->post('jumlah_pj1');
		$inis1 = $this->input->post('inis_1');
		$inis2 = $this->input->post('inis_2');
		$data1[0] = array
		(
			$inis1 => $jp,
			$inis2 => $jp1,
		);
		$a = json_encode($data1);
		$data2 = array(
			$this->input->post('kk') => $a,
		);

		$tahun_mulai = $this->input->post('tahun_mulai');
		$tahun_sampai = $this->input->post('tahun_sampai');
		$selisih = $tahun_sampai-$tahun_mulai;
		$data = array(
			'tahun' => $tahun_mulai.'-'.$tahun_sampai,
			'no1' => $tahun_mulai,
			'no2' => ($selisih+1)*2,
			'no3' => $selisih+1,
			'no4' => $tahun_sampai,
			'hal1' => 'Pejabat Fungsional Pengembangan Kompetensi',
			'hal2' => '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pengembangan Kompetensi</li>',
			'pengembangan' => $this->db->query('SELECT pk.*,jb.nama_jabatan,ins.nama_instansi from pengembangan_kompetensi pk join jabatan jb on pk.id_jabatan=jb.id_jabatan join instansi ins on pk.id_instansi=ins.id_instansi group by pk.id_jabatan')->result(),
			'instansi' => $this->db->get('instansi')->result(),
			'jabatan' => $this->db->get('jabatan')->result(),
			'page' => 'kompetensi/pengembangan_kompetensi',
		);
		if ($kk == "dik_lain")
		{
			$this->db->update('pengembangan_kompetensi',array('dik_lain'=>$jp),array('id_instansi'=>$instansi,'id_jabatan'=>$jabatan,'tahun'=>$th));
			$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data!", "Klik OK untuk menutup alert!!", "success");</script>');
		}
		else
		{
			$this->db->update('pengembangan_kompetensi',$data2,array('id_instansi'=>$instansi,'id_jabatan'=>$jabatan,'tahun'=>$th));
			$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data!", "Klik OK untuk menutup alert!!", "success");</script>');
		}
		$this->load->view('dashboard',$data);
	}
	// public function update_pejafung_pk1()
	// {
	// 	$id = $this->input->post('id_pengembangan');
	// 	$des = $this->input->post('deskripsi');
	// 	$instansi = $this->input->post('instansi');
	// 	$jabatan = $this->input->post('jabatan');
	// 	$tahun = $this->input->post('tahun');
	// 	$data1 = array(
	// 		'komp_tek' => $des,
	// 	);
	// 	$data2 = array(
	// 		'kompetensi_teknis' => $des,
	// 	);
	// 	$data3 = array(
	// 		'id_instansi' => $instansi,
	// 		'id_jabatan' => $jabatan,
	// 		'tahun' => $tahun,
	// 	);
	// 	$this->db->update('pengembangan_kompetensi',$data1,array('id_pengembangan'=>$id));
	// 	$this->db->update('profil_jf',$data2,$data3);

	// 	$tahun_mulai = $this->input->post('tahun_mulai_');
	// 	$tahun_sampai = $this->input->post('tahun_sampai_');
	// 	$selisih = $tahun_sampai-$tahun_mulai;
	// 	$data = array(
	// 		'tahun' => $tahun_mulai.'-'.$tahun_sampai,
	// 		'no1' => $tahun_mulai,
	// 		'no2' => ($selisih+1)*2,
	// 		'no3' => $selisih+1,
	// 		'no4' => $tahun_sampai,
	// 		'hal1' => 'Pejabat Fungsional Pengembangan Kompetensi',
	// 		'hal2' => '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pengembangan Kompetensi</li>',
	// 		'pengembangan' => $this->db->query('SELECT pk.*,jb.nama_jabatan,ins.nama_instansi from pengembangan_kompetensi pk join jabatan jb on pk.id_jabatan=jb.id_jabatan join instansi ins on pk.id_instansi=ins.id_instansi')->result(),
	// 		'instansi' => $this->db->get('instansi')->result(),
	// 		'jabatan' => $this->db->get('jabatan')->result(),
	// 		'page' => 'kompetensi/pengembangan_kompetensi',
	// 	);
	// 	$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data!", "Klik OK untuk menutup alert!!", "success");</script>');
	// 	$this->load->view('dashboard',$data);
	// }
	// public function tambah_pejafung_pk1()
	// {
	// 	$tahun = $this->input->post('tahun');
	// 	$cek_jafung = $this->db->get_where('pengembangan_kompetensi',array('tahun'=>$tahun))->num_rows();
	// 	if ($cek_jafung==0)
	// 	{
	// 		$jafung = $this->db->get_where('pengembangan_kompetensi',array('tahun'=>date('Y')))->result();
	// 		foreach ($jafung as $key => $jf) 
	// 		{
	// 			$data = array(
	// 				'id_jabatan' => $jf->id_jabatan,
	// 				'id_instansi' => $jf->id_instansi,
	// 				'tahun' => $tahun
	// 			);
	// 			$this->db->insert('pengembangan_kompetensi',$data);
	// 		}
	// 		$this->session->set_flashdata('alert','<script>swal("Berhasil Menambahkan data!", "Klik OK untuk menutup alert!!", "success");</script>');
	// 		redirect('Kompetensi');
	// 	}
	// 	else
	// 	{
	// 		$this->session->set_flashdata('alert','<script>swal("Mohon Maaf Data Sudah Ada!", "Klik OK untuk menutup alert!!", "error");</script>');
	// 		redirect('Kompetensi');
	// 	}
	// }
	public function tambah_pejafung_pk2()
	{
		$id=$this->input->post('id');
		$jb=$this->input->post('jabatan');
		$ins=$this->input->post('instansi');
		$kk = $this->input->post('kk');
		$jp = $this->input->post('jumlah_pj');
		$th = $this->input->post('tahun');
		$inis1 = $this->input->post('inis_1');
		$inis2 = $this->input->post('inis_2');
		$data1[0] = array
		(
			$inis1 => $jp,
			$inis2 => 0,
		);
		$a = json_encode($data1);
		$data2 = array(
			$kk => $a,
			'tahun' => $th,
			'id_jabatan' => $jb,
			'id_instansi' => $ins
		);
		$this->db->insert('pengembangan_kompetensi',$data2);

		$tahun_mulai = $this->input->post('tahun_mulai');
		$tahun_sampai = $this->input->post('tahun_sampai');
		$selisih = $tahun_sampai-$tahun_mulai;
		$data = array(
			'tahun' => $tahun_mulai.'-'.$tahun_sampai,
			'no1' => $tahun_mulai,
			'no2' => ($selisih+1)*2,
			'no3' => $selisih+1,
			'no4' => $tahun_sampai,
			'hal1' => 'Pejabat Fungsional Pengembangan Kompetensi',
			'hal2' => '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pengembangan Kompetensi</li>',
			'pengembangan' => $this->db->query('SELECT pk.*,jb.nama_jabatan,ins.nama_instansi from pengembangan_kompetensi pk join jabatan jb on pk.id_jabatan=jb.id_jabatan join instansi ins on pk.id_instansi=ins.id_instansi group by pk.id_jabatan')->result(),
			'instansi' => $this->db->get('instansi')->result(),
			'jabatan' => $this->db->get('jabatan')->result(),
			'page' => 'kompetensi/pengembangan_kompetensi',
		);
		$this->session->set_flashdata('alert','<script>swal("Berhasil Menambahkan data!", "Klik OK untuk menutup alert!!", "success");</script>');
		$this->load->view('dashboard',$data);
	}
}
?>