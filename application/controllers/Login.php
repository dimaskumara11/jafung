<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak == "admin" OR $hak == "kementrian")
 		{
 			redirect('Admin');
 		}
 		// else if ($hak == "kementrian")
 		// {
 		// 	redirect('Kementrian');
 		// }
	}

	public function index()
	{
		$this->load->view('login.php');
	}
	public function aksi_login()
	{
		$nik = $this->input->post('nik');
		$password = $this->input->post('password');
		$data = array
		(
			'nik' => $nik,
			'password' => md5($password)
		);
		$cek = $this->db->get_where("user",$data)->num_rows();
		if($cek > 0)
		{
			$where = array('nik'=>$nik);
 			$tampil = $this->db->get_where("user",$where)->row();
				$data_session = array(
					'nik' => $tampil->nik,
					'id_user' => $tampil->id_user,
					'nama' => $tampil->nama_user,
					'nama_kementrian' => $tampil->nama_kementrian,
					'password' => $password,
					'hak_akses' => $tampil->hak_akses,
					'email' => $tampil->email
					);
			$this->session->set_userdata($data_session);
			redirect('Login');
		}
		else
		{
			$this->session->set_flashdata('alert','<script>swal("Gagal Login!", "Username Dan Password Tidak Valid", "error");</script>');
			$this->load->view('login.php');
		}
	}
}
?>