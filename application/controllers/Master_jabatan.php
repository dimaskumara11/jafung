<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_jabatan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$hak = $this->session->userdata('hak_akses');
		$data['hal1'] = 'Jabatan';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Jabatan</li>';
		$data['title'] = "";
		$data['page'] = "master/master_jabatan";
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->load->view('dashboard',$data);
	}
	public function tampil_jabatan()
	{
		$hak = $this->session->userdata('hak_akses');
		$data = $this->db->get('jabatan')->result();
		echo json_encode($data);
	}

	public function tambah_jabatan()
	{
		$hak = $this->session->userdata('hak_akses');
		$jabatan = $this->input->post('nama_jabatan');
		$tambah_jabatan = $this->db->insert('jabatan',array('nama_jabatan'=>$jabatan,));
		echo json_encode($jabatan);
	}

	public function edit_jabatan($id)
	{
		$hak = $this->session->userdata('hak_akses');
		$data = $this->db->query('select * from jabatan where id_jabatan = '.$id.'')->row_array();
		echo json_encode($data);
	}

	public function delete_jabatan($id)
	{
		$hak = $this->session->userdata('hak_akses');
		$this->db->delete('jabatan',array('id_jabatan' => $id));
		echo $hak;
	}
	public function update_jabatan()
	{
		$hak = $this->session->userdata('hak_akses');
		$id = $this->input->post('id_jabatan');
		$jabatan = $this->input->post('nama_jabatan');
		$data1 = array(
			'nama_jabatan' => $jabatan,
		);
		$this->db->update('jabatan',$data1,array('id_jabatan'=>$id));
		echo $hak;
	}

}
?>