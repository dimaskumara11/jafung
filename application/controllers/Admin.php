<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$hak = $this->session->userdata('hak_akses');
		$data['hal1'] = 'Dashboard';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Dashboard</li>';
		$data['page'] = 'halaman_admin1';
		$data['instansi'] = $this->db->get('instansi')->result();
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('pejafung pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$this->db->group_by('pj.id_jabatan');
	 		if ($hak != "admin")
	 		{
	 			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
	 		}
		$data['pejafung'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}
	public function tampiljabatan()
	{
		$data = $this->db->get('jabatan')->result();
		echo json_encode($data);
	}

	public function tambah_tahun_pejafung()
	{
		$tahun = $this->input->post('tahun');
		$pejafung = $this->input->post('pejafung');
		$hak = $this->session->userdata('hak_akses');


		if ($pejafung=="PROYEKSI")
		{
	 		if ($hak != "admin")
	 		{
				$cek_jafung = $this->db->get_where('proyeksi',array('tahun'=>$tahun,'id_instansi'=>$this->session->userdata('nama_kementrian')))->num_rows();
	 		}
	 		else
	 		{
				$cek_jafung = $this->db->get_where('proyeksi',array('tahun'=>$tahun))->num_rows();
	 		}

			if ($cek_jafung==0)
			{
		 		if ($hak != "admin")
		 		{
					$jafung = $this->db->get_where('proyeksi',array('tahun'=>date('Y'),'id_instansi'=>$this->session->userdata('nama_kementrian')))->result();
		 		}
		 		else
		 		{
					$jafung = $this->db->get_where('proyeksi',array('tahun'=>date('Y')))->result();
		 		}
				// print_r($jafung);die();
				foreach ($jafung as $key => $jf) 
				{
					$data = array(
						'id_jabatan' => $jf->id_jabatan,
						'id_instansi' => $jf->id_instansi,
						'tahun' => $tahun
					);
					$this->db->insert('proyeksi',$data);
				}
			$this->session->set_flashdata('alert','<script>swal("Berhasil Menambahkan Data Pejafung!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Admin/pejafung_'.$pejafung);
			}
			else
			{
				$this->session->set_flashdata('alert','<script>swal("Gagal Menambahkan Data Pejafung!", "Klik OK untuk menutup alert!!", "error");</script>');
				redirect('Admin/pejafung_'.$pejafung);
			}
		}
		else
		{
	 		if ($hak != "admin")
	 		{
				$cek_jafung = $this->db->get_where('pejafung',array('tahun'=>$tahun,'id_instansi'=>$this->session->userdata('nama_kementrian')))->num_rows();
	 		}
	 		else
	 		{
				$cek_jafung = $this->db->get_where('pejafung',array('tahun'=>$tahun))->num_rows();
	 		}

			if ($cek_jafung==0)
			{
		 		if ($hak != "admin")
		 		{
					$jafung = $this->db->get_where('pejafung',array('tahun'=>date('Y'),'id_instansi'=>$this->session->userdata('nama_kementrian')))->result();
		 		}
		 		else
		 		{
					$jafung = $this->db->get_where('pejafung',array('tahun'=>date('Y')))->result();
		 		}
				// print_r($jafung);die();
				foreach ($jafung as $key => $jf) 
				{
					$data = array(
						'id_jabatan' => $jf->id_jabatan,
						'id_instansi' => $jf->id_instansi,
						'tahun' => $tahun
					);
					$this->db->insert('pejafung',$data);
				}
			$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data Pejafung!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Admin/pejafung_'.$pejafung);
			}
			else
			{
				$this->session->set_flashdata('alert','<script>swal("Gagal Menambahkan Data Pejafung!", "Klik OK untuk menutup alert!!", "error");</script>');
				redirect('Admin/pejafung_'.$pejafung);
			}
		}
	}


	public function struktural()
	{
		$data['hal1'] = 'Maintenance';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Maintenance</li>';
		$data['page'] = 'maintenance';
		$this->load->view('dashboard',$data);
	}
	public function informasi()
	{
		$data['hal1'] = 'Maintenance';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Maintenance</li>';
		$data['page'] = 'maintenance';
		$this->load->view('dashboard',$data);
	}
	public function monitoring()
	{
		$data['hal1'] = 'Maintenance';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Maintenance</li>';
		$data['page'] = 'maintenance';
		$this->load->view('dashboard',$data);
	}
	public function layanan()
	{
		$data['hal1'] = 'Maintenance';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Maintenance</li>';
		$data['page'] = 'maintenance';
		$this->load->view('dashboard',$data);
	}
	public function operator()
	{
		$data['hal1'] = 'Maintenance';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Maintenance</li>';
		$data['page'] = 'maintenance';
		$this->load->view('dashboard',$data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Login');
	}
}
