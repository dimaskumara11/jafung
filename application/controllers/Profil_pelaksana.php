<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_pelaksana extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$data['tahun'] = $this->input->post('tahun');
		if ($data['tahun']==0)
		{
			$data['tahun'] = date('Y');
		}
		$data['hal1'] = 'Profile Pelaksana';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Profil Pelaksana</li>';
		$data['page'] = 'profil/profil_pelaksana';
		$this->db->select('*');
		$this->db->from('profil_pelaksana pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$data['profil'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}
	public function halaman($tahun)
	{
		$data['tahun'] = $tahun;
		$data['hal1'] = 'Profile Pelaksana';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Profil Pelaksana</li>';
		$data['page'] = 'profil/profil_pelaksana';
		$this->db->select('*');
		$this->db->from('profil_pelaksana pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$data['profil'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}
	public function edit_profil_pelaksana($id)
	{
		$id1 = $this->input->post('id1');
		$row = $this->db->get_where('profil_pelaksana',array('id_profil_pelaksana'=>$id1))->row();
		$a = $row->$id;
		echo $a;
	}
	public function update_profil_pelaksana()
	{
		$tahun=$this->input->post('tahun');
		$id=$this->input->post('idpelaksana');
		$tr = $this->input->post('target');
		$des = $this->input->post('deskripsi');
		$this->db->update('profil_pelaksana',array($tr=>$des),array('id_profil_pelaksana'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data Jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Profil_pelaksana/halaman/'.$tahun);
	}
}
?>