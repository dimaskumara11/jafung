<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$data['hal1'] = 'Forum Diskusi';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Forum Diskusi</li>';
		$data['page'] = "forum/forum";
		$this->db->select('*')->from('forum fr')->join('user u','u.id_user=fr.id_user')->order_by('fr.id','DESC');
		$data['forum'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}
	public function edit_view_forum()
	{
		$data['hal1'] = 'Forum Diskusi';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Forum Diskusi</li>';
		$data['page'] = "forum/edit_forum";
		$data['forum'] = $this->db->get_where('forum',array('id'=>$this->input->post('id')))->row();
		// print_r($data['forum']);die();
		$data['id'] = $this->input->post('id');
		$data['act'] = "update";
		$this->load->view('dashboard',$data);
	}
	public function tambah_view_forum()
	{
		$data['hal1'] = 'Forum Diskusi';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Forum Diskusi</li>';
		$data['page'] = "forum/edit_forum";
		$data['id'] = '';
		$data['act'] = "simpan";
		$this->load->view('dashboard',$data);
	}
	public function edit_forum()
	{
		$update = $this->db->update('forum',array('isi'=>$this->input->post('forum')),array('id'=>$this->input->post('id')));
		if ($update)
		{
			$this->session->set_flashdata('alert','<script>swal("Berhasil Sunting Posting!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Forum');
		}
		else
		{
			$this->session->set_flashdata('alert','<script>swal("Sunting Posting Gagal!", "Klik OK untuk menutup alert!!", "error");</script>');
			redirect('Forum');
		}
	}
	public function simpan_forum()
	{
		$simpan = $this->db->insert('forum',array('isi'=>$this->input->post('forum'),'id_user'=>$this->session->userdata('id_user')));
		if ($simpan)
		{
			$this->session->set_flashdata('alert','<script>swal("Postingan Berhasil Di Unggah!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Forum');
		}
		else
		{
			$this->session->set_flashdata('alert','<script>swal("Postingan Gagal!", "Klik OK untuk menutup alert!!", "error");</script>');
			redirect('Forum');
		}
	}
	public function delete_forum($id)
	{
		$delete = $this->db->delete('forum',array('id'=>$id));
		echo json_encode($id);
	}
}
?>