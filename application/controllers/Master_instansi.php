<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_instansi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$hak = $this->session->userdata('hak_akses');
		$data['hal1'] = 'Instansi Pembina';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Instansi Pembina</li>';
		$data['title'] = "Tambah Pejabat Fungsional";
		$data['page'] = "master/master_instansi";
		$data['instansi'] = $this->db->get('instansi')->result();
		$this->load->view('dashboard',$data);
	}
	public function tampil_instansi()
	{
		$hak = $this->session->userdata('hak_akses');
		$data = $this->db->get('instansi')->result();
		echo json_encode($data);
	}

	public function tambah_instansi()
	{
		$hak = $this->session->userdata('hak_akses');
		$instansi = $this->input->post('nama_instansi');
		$tambah_instansi = $this->db->insert('instansi',array('nama_instansi'=>$instansi));
		echo json_encode($instansi);
	}

	public function edit_instansi($id)
	{
		$hak = $this->session->userdata('hak_akses');
		$data = $this->db->query('select * from instansi where id_instansi = '.$id.'')->row_array();
		echo json_encode($data);
	}

	public function delete_instansi($id)
	{
		$hak = $this->session->userdata('hak_akses');
		$this->db->delete('instansi',array('id_instansi' => $id));
		echo $hak;
	}
	public function update_instansi()
	{
		$hak = $this->session->userdata('hak_akses');
		$id = $this->input->post('id_instansi');
		$instansi = $this->input->post('nama_instansi');
		$data1 = array(
			'nama_instansi' => $instansi,
		);
		$this->db->update('instansi',$data1,array('id_instansi'=>$id));
		echo $hak;
	}

}
?>