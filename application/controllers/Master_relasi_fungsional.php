<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_relasi_fungsional extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function tampiljabatan()
	{
		$data = $this->db->get('jabatan')->result();
		echo json_encode($data);
	}
	public function index()
	{
		$data['hal1'] = 'Relasi Fungsional';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Relasi Fungsional</li>';
		$data['title'] = "";
		$data['page'] = "master_relasi/relasi_jafung";
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$data['instansi'] = $this->db->get_where('instansi',array('id_instansi'=>$this->session->userdata('nama_kementrian')))->result();
 		}
 		else
 		{
			$data['instansi'] = $this->db->get('instansi')->result();
 		}
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('relasi pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
 		if ($hak != "admin")
 		{
 			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$this->db->order_by('tahun', 'DESC');
		$this->db->group_by('pj.id_instansi');
		$data['relasi'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}

	public function tambah_relasi()
	{
		$hak = $this->session->userdata('hak_akses');
		$kementrian = $this->input->post('nama_kementrian');
		$jabatan = $this->input->post('nama_jabatan');
		$tahun = $this->input->post('tahun');	
		foreach ($jabatan as $key => $jbt) 
		{
			$data2 = array(
				'id_jabatan'=> $jbt,
				'id_instansi'=>$kementrian,
				'tahun'=>$tahun
			);
			$cek_relasi = $this->db->get_where('relasi',$data2)->num_rows();
			if($cek_relasi == 0)
			{
				$this->db->insert('relasi',$data2);
				$cek_peraturan = $this->db->get_where('peraturan_pelaksanaan',$data2)->num_rows();
				if($cek_peraturan == 0)
				{
					$this->db->insert('peraturan_pelaksanaan',$data2);
				}
				$cek_proyeksi = $this->db->get_where('proyeksi',$data2)->num_rows();
				if($cek_proyeksi == 0)
				{
					$this->db->insert('proyeksi',$data2);
				}
				$cek_pejafung = $this->db->get_where('pejafung',$data2)->num_rows();
				if($cek_pejafung == 0)
				{
					$this->db->insert('pejafung',$data2);
				}
				$cek_kompetensi = $this->db->get_where('pengembangan_kompetensi',$data2)->num_rows();
				if($cek_kompetensi == 0)
				{
					$this->db->insert('pengembangan_kompetensi',$data2);
				}
				$cek_profil_jf = $this->db->get_where('profil_jf',$data2)->num_rows();
				if($cek_profil_jf == 0)
				{
					$this->db->insert('profil_jf',$data2);
				}
			}
			else
			{
				$this->session->set_flashdata('alert','<script>swal("Mohon Maaf Data Sudah Ada!", "Klik OK untuk menutup alert!!", "error");</script>');
				redirect('Master_relasi_fungsional');
			}
		}	
		$this->session->set_flashdata('alert','<script>swal("Berhasil Menambahkan Relasi jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Master_relasi_fungsional');
	}

	public function delete_relasi($id)
	{
		$data2 = array('id_instansi'=>$id);
		$this->db->delete('relasi',$data2);
		$this->db->delete('peraturan_pelaksanaan',$data2);
		$this->db->delete('proyeksi',$data2);
		$this->db->delete('pejafung',$data2);
		$this->db->delete('pengembangan_kompetensi',$data2);
		$this->db->delete('profil_jf',$data2);
		echo $data2;
	}

	public function detail_relasi()
	{
		$data['hal1'] = 'Relasi Jafung';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Relasi Jafung</li>';
		$data['hal3'] = $this->input->post('nama_kementrian');
		$id = $this->input->post('id_instansi');
		$data['page'] = 'master_relasi/daftar_kementrian_tahun';
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$data['relasi'] = $this->db->query('select * from relasi where id_instansi = '.$id.' group by tahun')->result();
		$data['instansi'] = $id;
		$this->load->view('dashboard',$data);
	}

	public function detail_relasi_tahun($id,$tahun)
	{
		$data['hal1'] = 'Relasi Jafung';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Relasi Jafung</li>';
		$data['hal3'] = $tahun;
		$data['hal4'] = $id;
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$data['relasi'] = $this->db->query('select re.*,jb.nama_jabatan from relasi re join jabatan jb on re.id_jabatan = jb.id_jabatan where re.id_instansi = '.$id.' and tahun = '.$tahun.'')->result();
		$this->load->view('master_relasi/detail_relasi_tahun',$data);
	}

	public function edit_relasi_tahun($id,$tahun)
	{
		$data['hal1'] = 'Relasi Jafung';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Relasi Jafung</li>';
		$data['hal3'] = $tahun;
		$data['hal4'] = $id;
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$data['relasi'] = $this->db->query('select re.*,jb.nama_jabatan from relasi re join jabatan jb on re.id_jabatan = jb.id_jabatan where re.id_instansi = '.$id.' and tahun = '.$tahun.'')->result();
		$this->load->view('master_relasi/edit_detail_jafung',$data);
	}
	public function update_jafung_relasi()
	{
		$jf = $this->input->post('jafung');
		$th = $this->input->post('tahun1');
		$id = $this->input->post('id1');
		for($i=0;$i<count($jf);$i++)
		{
			$data2 = array(
				'id_instansi'=>$id,
				'tahun'=>$th,
				'id_jabatan'=>$jf[$i],
			);
			$this->db->delete('relasi',$data2);
			$this->db->delete('peraturan_pelaksanaan',$data2);
			$this->db->delete('proyeksi',$data2);
			$this->db->delete('pejafung',$data2);
			$this->db->delete('pengembangan_kompetensi',$data2);
			$this->db->delete('profil_jf',$data2);
		}
			$this->session->set_flashdata('alert','<script>swal("Berhasil Menghapus Data jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Master_relasi_fungsional');
	}

	public function tambah_jafung()
	{
		$tahun = $this->input->post('tahun');
		$instansi = $this->input->post('id_instansi');
		$jabatan = $this->input->post('nama_jabatan');
		foreach ($jabatan as $key => $jbt) 
		{
			$data2 = array(
				'id_jabatan'=> $jbt,
				'id_instansi'=>$instansi,
				'tahun'=>$tahun
			);
			$cek_relasi = $this->db->get_where('relasi',$data2)->num_rows();
			if($cek_relasi == 0)
			{
				$this->db->insert('relasi',$data2);
				$cek_peraturan = $this->db->get_where('peraturan_pelaksanaan',$data2)->num_rows();
				if($cek_peraturan == 0)
				{
					$this->db->insert('peraturan_pelaksanaan',$data2);
				}
				$cek_proyeksi = $this->db->get_where('proyeksi',$data2)->num_rows();
				if($cek_proyeksi == 0)
				{
					$this->db->insert('proyeksi',$data2);
				}
				$cek_pejafung = $this->db->get_where('pejafung',$data2)->num_rows();
				if($cek_pejafung == 0)
				{
					$this->db->insert('pejafung',$data2);
				}
				$cek_kompetensi = $this->db->get_where('pengembangan_kompetensi',$data2)->num_rows();
				if($cek_kompetensi == 0)
				{
					$this->db->insert('pengembangan_kompetensi',$data2);
				}
				$cek_profil_jf = $this->db->get_where('profil_jf',$data2)->num_rows();
				if($cek_profil_jf == 0)
				{
					$this->db->insert('profil_jf',$data2);
				}
			}
			else
			{
				$this->session->set_flashdata('alert','<script>swal("Mohon Maaf Data Sudah Ada!", "Klik OK untuk menutup alert!!", "error");</script>');
				redirect('Master_relasi_fungsional');
			}
		}
		$this->session->set_flashdata('alert','<script>swal("Berhasil Menambahkan jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Master_relasi_fungsional');
	}

	public function copy_jafung()
	{
		$tahun = $this->input->post('tahun');
		$tahun_ = $this->input->post('tahun_');
		$instansi = $this->input->post('id_instansi');
		$data1 = array(
			'id_instansi' => $instansi,
			'tahun' => $tahun,
		);
		$data1_ = array(
			'id_instansi' => $instansi,
			'tahun' => $tahun_,
		);
		$cek_relasi = $this->db->get_where('relasi',$data1)->num_rows();
		if($cek_relasi == 0)
		{
			$detail_jafung1 = $this->db->get_where('relasi',$data1_)->result();
			// print_r($detail_jafung1);die();
			foreach ($detail_jafung1 as $key => $dj) 
			{
				$data2 = array(
					'id_instansi' => $instansi,
					'id_jabatan' => $dj->id_jabatan,
					'tahun' => $tahun
				);
				$this->db->insert('relasi',$data2);
				$cek_peraturan = $this->db->get_where('peraturan_pelaksanaan',$data2)->num_rows();
				if($cek_peraturan == 0)
				{
					$this->db->insert('peraturan_pelaksanaan',$data2);
				}
				$cek_proyeksi = $this->db->get_where('proyeksi',$data2)->num_rows();
				if($cek_proyeksi == 0)
				{
					$this->db->insert('proyeksi',$data2);
				}
				$cek_pejafung = $this->db->get_where('pejafung',$data2)->num_rows();
				if($cek_pejafung == 0)
				{
					$this->db->insert('pejafung',$data2);
				}
				$cek_kompetensi = $this->db->get_where('pengembangan_kompetensi',$data2)->num_rows();
				if($cek_kompetensi == 0)
				{
					$this->db->insert('pengembangan_kompetensi',$data2);
				}
				$cek_profil_jf = $this->db->get_where('profil_jf',$data2)->num_rows();
				if($cek_profil_jf == 0)
				{
					$this->db->insert('profil_jf',$data2);
				}
			}
			$this->session->set_flashdata('alert','<script>swal("Berhasil Mengcopy jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Master_relasi_fungsional');
		}
		else
		{
			$this->session->set_flashdata('alert','<script>swal("Mohon Maaf Data Sudah Ada!", "Klik OK untuk menutup alert!!", "error");</script>');
			redirect('Master_relasi_fungsional');
		}
	}
	public function update_relasi()
	{
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
 			redirect('Kementrian');
 		}
 		
		$id = $this->input->post('id_relasi');
		$jabatan = $this->input->post('nama_jabatan');
		$kementrian = $this->input->post('nama_kementrian');
		$data1 = array(
			'id_jabatan' => $jabatan,
			'id_instansi' => $kementrian,
		);
		$this->db->update('relasi',$data1,array('id_relasi'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data Pejafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Master_relasi_fungsional');
	}

}
?>