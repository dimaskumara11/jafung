<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
 	}
	public function index()
	{
		$data['hal1'] = 'Profil';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Profil</li>';
		$data['page'] = "profil/profil";
		$data['profil'] = $this->db->get_where('user',array('nik'=>$this->session->userdata('nik')))->row();
		$this->load->view('dashboard',$data);
	}
	public function update_profil()
	{
		$id = $this->input->post('id_pengguna');
		$nama = $this->input->post('nama_pengguna');
		$email = $this->input->post('email');
		$no_hp = $this->input->post('no_hp');
		$data1 = array(
			'nama_user' => $nama,
			'email' => $email,
			'no_hp' => $no_hp
		);
		$this->db->update('user',$data1,array('id_user'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Update Nama & Kontak Profil Berhasil!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Profil');
	}
	public function update_password()
	{
		$id = $this->input->post('id_pengguna');
		$pass = $this->input->post('password');
		$data1 = array(
			'password' => md5($pass),
		);
		$this->db->update('user',$data1,array('id_user'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Update Password Akun Berhasil!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Profil');
	}
	
}
?>