<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelaksana extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
 			redirect('Login');
 		}
 	}
	public function index()
	{
		$data['tahun'] = $this->input->post('tahun');
		if ($data['tahun']==0)
		{
			$data['tahun'] = date('Y');
		}
		$data['hal1'] = 'Pelaksana';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pelaksana</li>';
		$data['page'] = 'pelaksana/pelaksana';
		$this->db->select('*');
		$this->db->from('pelaksana pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$data['pelaksana'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}
	public function halaman($tahun)
	{
		$data['tahun'] = $tahun;
		$data['hal1'] = 'Pelaksana';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pelaksana</li>';
		$data['page'] = 'pelaksana/pelaksana';
		$this->db->select('*');
		$this->db->from('pelaksana pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$data['pelaksana'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}
	public function edit_pelaksana($id)
	{
		$id1 = $this->input->post('id1');
		$id2 = $this->input->post('id2');
		$row = $this->db->get_where('pelaksana',array('id_pelaksana'=>$id))->row();
		$a = json_decode($row->$id2);
		$b = $a[0]->$id1;
		echo $b;
	}
	public function update_jafung_pelaksana()
	{
		$tahun=$this->input->post('tahun');
		$id=$this->input->post('idpelaksana');
		$pd = $this->input->post('pendidikan');
		$us = $this->input->post('usia');
		$jml = $this->input->post('jumlah');
		$query1 = $this->db->get_where('pelaksana',array('id_pelaksana'=>$id))->row();
		$a = json_decode($query1->$pd);
		$b = $a[0]->$us;
		if($us == "usiaatas50")
		{
			$c = array(
				$us => $jml,
				'usiabawah50' => $a[0]->usiabawah50
			);
		}
		else 
		{
			$c = array(
				'usiaatas50' => $a[0]->usiaatas50,
				$us => $jml,
			);
		}
		$data = array(
			$pd => '['.json_encode($c).']',
		);
		// print_r($c);die();
		$this->db->update('pelaksana',$data,array('id_pelaksana'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data Jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Pelaksana/halaman/'.$tahun);
	}
	public function update_jafung_pelaksana1()
	{
		$id = $this->input->post('id_pelaksana');
		$des = $this->input->post('deskripsi');
		$data1 = array(
			'urusan' => $des,
		);
		$this->db->update('pelaksana',$data1,array('id_pelaksana'=>$id));

		$data['tahun'] = $this->input->post('tahun');
		if ($data['tahun']==0)
		{
			$data['tahun'] = date('Y');
		}
		$data['hal1'] = 'Pelaksana';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Pelaksana</li>';
		$data['page'] = 'pelaksana/pelaksana';
		$this->db->select('*');
		$this->db->from('pelaksana pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$data['pelaksana'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}
	}