<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_jf extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
        $this->load->helper(array('form', 'url'));
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$data['tahun'] = $this->input->post('tahun');
		if ($data['tahun']==0)
		{
			$data['tahun'] = date('Y');
		}
		$data['hal1'] = 'Profile JF';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Profil JF</li>';
		$data['page'] = 'profil/profil_jf';
		$this->db->select('*');
		$this->db->from('profil_jf pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		// $hak = $this->session->userdata('hak_akses');
 	// 	if ($hak != "admin")
 	// 	{
		// 	$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 	// 	}
		$data['profil'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}

	public function halaman($tahun)
	{
		$data['tahun'] = $tahun;
		$data['hal1'] = 'Profile JF';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Profil JF</li>';
		$data['page'] = 'profil/profil_jf';
		$this->db->select('*');
		$this->db->from('profil_jf pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$data['profil'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}

	public function update_profil_jf()
	{
		$input=$this->input->post();
		$kategori = json_encode($input['kategori']);
		$jenjang_ket = json_encode($input['jenjang_jabatan_keterampilan']);
		$jenjang_keah = json_encode($input['jenjang_jabatan_keahlian']);

		$permenpanrb = $input['permenpanrb'];
		$junlak_junkis = $input['junlak_junkis'];
		$tugas_pokok = $input['tugas_pokok'];
		$perpres_tunjangan = $input['perpres_tunjangan'];
		$pengaturan_bup = $input['pengaturan_bup'];
		// $upload_file = 
		// 	$_FILES['pengangkatan_pertama','pengangkatan_penyesuaian','perpndahan_jabatan_lain','promosi','penialaian_kerja','pemberhentian','pengangkatan_kembali','jenjang_tunjangan_fungsional'];

		$jenjang = array();
		if(!empty($jenjang_ket))
		{
			$push_jenjang_ket['keterampilan'] = $jenjang_ket;
			array_push($jenjang, $push_jenjang_ket);
		}

		if(!empty($jenjang_keah))
		{
			$push_jenjang_keah['keahlian'] = $jenjang_keah;
			array_push($jenjang, $push_jenjang_keah);
		}
		foreach ($_FILES as $key => $value) 
		{
			@mkdir('assets/file_dokumen/'.date('Y').'/'.date('m').'/'.$key, 0777, TRUE);
			$path = date('Y').'/'.date('m').'/'.$key;
			$dt_file = '<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>';
			if(!file_exists('assets/file_dokumen/'.date('Y').'/'.date('m').'/'.$key.'/index.html')){
				$fh = fopen('assets/file_dokumen/'.date('Y').'/'.date('m').'/'.$key.'/index.html', 'w+');
				fwrite($fh, $dt_file);
			}

			//upload_file
			$config['upload_path']          = './assets/file_dokumen'.$path.'/';
        	$config['file_name']     		= $value['name'];
	        $config['allowed_types']        = '*';
	        $config['max_size']             = 1000000;

	        $this->load->library('upload', $config);
	        // print_r($config);die();
			$this->upload->initialize($config);

	        if ( ! $this->upload->do_upload($key))
	        {
	                $error = array('error' => $this->upload->display_errors());
	                print_r($error);
	        }
	        else
	        {
	                $data = array('upload_data' => $this->upload->data());
	                print_r($data);
	        }
	        die();
		}

		$data = 
		[
			'kompetensi_teknis' => $input['kompetensi_teknis'],
			'kategori' => $kategori,
			'jenjang_jabatan' => json_encode($jenjang),
			'kedudukan' => json_encode($input['kedudukan'])
		];
		$data1 = array(
			'komp_tek' => $input['kompetensi_teknis'],
		);
		$data2 = array(
			'id_instansi' => $input['instansi'],
			'id_jabatan' => $input['jabatan'],
			'tahun' => $input['tahun'],
		);
		// print_r($data2);die();
		$this->db->update('pengembangan_kompetensi',$data1,$data2);
		$this->db->update('profil_jf',$data,['id_profil_jf'=>$input['id_profil_jf']]);
		$this->session->set_flashdata('alert','<script>swal("Berhasil Update Data!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Profil_jf/halaman/'.$input['tahun']);
	}

	public function edit_profil_jf($id)
	{
		$data = $this->db->get_where('profil_jf',['id_profil_jf'=>$id])->row();
		print_r(json_encode($data));
	}
}
?>