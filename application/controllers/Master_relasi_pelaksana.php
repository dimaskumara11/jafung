<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_relasi_pelaksana extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
	}
	public function tampiljabatan()
	{
		$data = $this->db->get('jabatan')->result();
		echo json_encode($data);
	}
	public function index()
	{
		$data['hal1'] = 'Relasi Pelaksana';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Relasi Pelaksana</li>';
		$data['title'] = "";
		$data['page'] = "master_relasi/relasi_jafung_pelaksana";
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$data['instansi'] = $this->db->get_where('instansi',array('id_instansi'=>$this->session->userdata('nama_kementrian')))->result();
 		}
 		else
 		{
			$data['instansi'] = $this->db->get('instansi')->result();
 		}
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('relasi_pelaksana pj');
		$this->db->join('jabatan jb', 'pj.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pj.id_instansi = ins.id_instansi');
 		if ($hak != "admin")
 		{
 			$this->db->where('pj.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$this->db->order_by('tahun', 'DESC');
		$this->db->group_by('pj.id_instansi');
		$data['relasi'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}

	public function tambah_relasi()
	{
		$hak = $this->session->userdata('hak_akses');
		$kementrian = $this->input->post('nama_kementrian');
		$jabatan = $this->input->post('nama_jabatan');
		$tahun = $this->input->post('tahun');	
		$urusan = $this->input->post('urusan');	
		foreach ($jabatan as $key => $jbt) 
		{
			$data2 = array(
				'id_jabatan'=> $jbt,
				'id_instansi'=>$kementrian,
				'tahun'=>$tahun,
				'urusan'=>$urusan[$key]
			);
			$data3 = array(
				'id_jabatan'=> $jbt,
				'id_instansi'=>$kementrian,
				'tahun'=>$tahun
			);
			$cek_relasi = $this->db->get_where('relasi_pelaksana',$data3)->num_rows();
			if($cek_relasi == 0)
			{
				$cek_pelaksana = $this->db->get_where('pelaksana',$data3)->num_rows();
				if($cek_pelaksana == 0)
				{
					$this->db->insert('pelaksana',$data2);
				}
				$cek_prof_pel = $this->db->get_where('profil_pelaksana',$data3)->num_rows();
				if($cek_prof_pel == 0)
				{
					$this->db->insert('profil_pelaksana',$data2);
				}
				$this->db->insert('relasi_pelaksana',$data2);
			}
			else
			{
				$this->session->set_flashdata('alert','<script>swal("Mohon Maaf Data Sudah Ada!", "Klik OK untuk menutup alert!!", "error");</script>');
				redirect('Master_relasi_pelaksana');
			}
		}	
		$this->session->set_flashdata('alert','<script>swal("Berhasil Menambahkan Relasi jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Master_relasi_pelaksana');
	}

	public function delete_relasi($id)
	{
		$data2 = array('id_instansi'=>$id);
		$this->db->delete('relasi_pelaksana',$data2);
		$this->db->delete('pelaksana',$data2);
		$this->db->delete('profil_pelaksana',$data2);
		echo $data2;
	}

	public function detail_relasi()
	{
		$data['hal1'] = 'Relasi Jafung';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Relasi Jafung</li>';
		$data['hal3'] = $this->input->post('nama_kementrian');
		$id = $this->input->post('id_instansi');
		$data['page'] = 'master_relasi/daftar_kementrian_tahun_pelaksana';
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$data['relasi'] = $this->db->query('select * from relasi_pelaksana where id_instansi = '.$id.' group by tahun')->result();
		$data['instansi'] = $id;
		$this->load->view('dashboard',$data);
	}

	public function detail_relasi_tahun($id,$tahun)
	{
		$data['hal1'] = 'Relasi Jafung';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Relasi Jafung</li>';
		$data['hal3'] = $tahun;
		$data['hal4'] = $id;
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$data['relasi'] = $this->db->query('select re.*,jb.nama_jabatan from relasi_pelaksana re join jabatan jb on re.id_jabatan = jb.id_jabatan where re.id_instansi = '.$id.' and tahun = '.$tahun.'')->result();
		$this->load->view('master_relasi/detail_relasi_tahun_pelaksana',$data);
	}

	public function edit_relasi_tahun($id,$tahun)
	{
		$data['hal1'] = 'Relasi Jafung';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Relasi Jafung</li>';
		$data['hal3'] = $tahun;
		$data['hal4'] = $id;
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$data['relasi'] = $this->db->query('select re.*,jb.nama_jabatan from relasi_pelaksana re join jabatan jb on re.id_jabatan = jb.id_jabatan where re.id_instansi = '.$id.' and tahun = '.$tahun.'')->result();
		$this->load->view('master_relasi/edit_detail_jafung_pelaksana',$data);
	}
	public function update_jafung_relasi()
	{
		$jf = $this->input->post('jafung');
		$th = $this->input->post('tahun1');
		$id = $this->input->post('id1');
		for($i=0;$i<count($jf);$i++)
		{
			$data2 = array(
				'id_instansi'=>$id,
				'tahun'=>$th,
				'id_jabatan'=>$jf[$i],
			);
			$this->db->delete('relasi_pelaksana',$data2);
			$this->db->delete('pelaksana',$data2);
			$this->db->delete('profil_pelaksana',$data2);
		}
			$this->session->set_flashdata('alert','<script>swal("Berhasil Menghapus Data jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Master_relasi_pelaksana');
	}

	public function tambah_jafung()
	{
		$tahun = $this->input->post('tahun');
		$instansi = $this->input->post('id_instansi');
		$jabatan = $this->input->post('nama_jabatan');
		$urusan = $this->input->post('urusan');
		foreach ($jabatan as $key => $jbt) 
		{
			$data2 = array(
				'id_jabatan'=> $jbt,
				'id_instansi'=>$instansi,
				'tahun'=>$tahun,
				'urusan'=>$urusan[$key]
			);
			$data3 = array(
				'id_jabatan'=> $jbt,
				'id_instansi'=>$instansi,
				'tahun'=>$tahun
			);
			$cek_relasi = $this->db->get_where('relasi_pelaksana',$data3)->num_rows();
			if($cek_relasi == 0)
			{
				$this->db->insert('relasi_pelaksana',$data2);
				$cek_pelaksana = $this->db->get_where('pelaksana',$data3)->num_rows();
				if($cek_pelaksana == 0)
				{
					$this->db->insert('pelaksana',$data2);
				}
				$cek_prof_pel = $this->db->get_where('profil_pelaksana',$data3)->num_rows();
				if($cek_prof_pel == 0)
				{
					$this->db->insert('profil_pelaksana',$data2);
				}
			}
			else
			{
				$this->session->set_flashdata('alert','<script>swal("Mohon Maaf Data Sudah Ada!", "Klik OK untuk menutup alert!!", "error");</script>');
				redirect('Master_relasi_pelaksana');
			}
		}
		$this->session->set_flashdata('alert','<script>swal("Berhasil Menambahkan jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Master_relasi_pelaksana');
	}

	public function copy_jafung()
	{
		$tahun = $this->input->post('tahun');
		$tahun_ = $this->input->post('tahun_');
		$instansi = $this->input->post('id_instansi');
		$data1 = array(
			'id_instansi' => $instansi,
			'tahun' => $tahun,
		);
		$data1_ = array(
			'id_instansi' => $instansi,
			'tahun' => $tahun_,
		);
		$cek_relasi = $this->db->get_where('relasi_pelaksana',$data1)->num_rows();
		if($cek_relasi == 0)
		{
			$detail_jafung1 = $this->db->get_where('relasi_pelaksana',$data1_)->result();
			foreach ($detail_jafung1 as $key => $dj) 
			{
				$data2 = array(
					'id_instansi' => $instansi,
					'id_jabatan' => $dj->id_jabatan,
					'tahun' => $tahun
				);
				$this->db->insert('relasi_pelaksana',$data2);
				$cek_pelaksana = $this->db->get_where('pelaksana',$data2)->num_rows();
				if($cek_pelaksana == 0)
				{
					$this->db->insert('pelaksana',$data2);
				}
				$cek_prof_pel = $this->db->get_where('profil_pelaksana',$data2)->num_rows();
				if($cek_prof_pel == 0)
				{
					$this->db->insert('profil_pelaksana',$data2);
				}
			}
			$this->session->set_flashdata('alert','<script>swal("Berhasil Mengcopy jafung!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Master_relasi_pelaksana');
		}
		else
		{
			$this->session->set_flashdata('alert','<script>swal("Mohon Maaf Data Sudah Ada!", "Klik OK untuk menutup alert!!", "error");</script>');
			redirect('Master_relasi_pelaksana');
		}
	}
	public function update_relasi()
	{
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
 			redirect('Kementrian');
 		}
 		
		$id = $this->input->post('id_relasi');
		$jabatan = $this->input->post('nama_jabatan');
		$kementrian = $this->input->post('nama_kementrian');
		$data1 = array(
			'id_jabatan' => $jabatan,
			'id_instansi' => $kementrian,
		);
		$this->db->update('relasi_pelaksana',$data1,array('id_relasi'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data Pejafung!", "Klik OK untuk menutup alert!!", "success");</script>');
		redirect('Master_relasi_pelaksana');
	}

}
?>