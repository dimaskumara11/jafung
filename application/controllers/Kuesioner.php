<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kuesioner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$data['hal1'] = 'kuesioner';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">kuesioner</li>';
		$data['page'] = "kuesioner/forum";
		$this->db->select('*')->from('forum fr')->join('user u','u.id_user=fr.id_user')->order_by('fr.id','DESC');
		$data['forum'] = $this->db->get()->result();
		$this->load->view('dashboard',$data);
	}
}
?>