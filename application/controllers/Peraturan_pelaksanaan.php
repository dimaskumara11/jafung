<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peraturan_pelaksanaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin" AND $hak != "kementrian")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$data['tahun'] = $this->input->post('tahun');
		if ($data['tahun']==0)
		{
			$data['tahun'] = date('Y');
		}
		$data['hal1'] = 'Pejabat Fungsional Peraturan Pelaksanaan';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Peraturan Pelaksanaan</li>';
		$data['instansi'] = $this->db->get('instansi')->result();
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('peraturan_pelaksanaan pp');
		$this->db->join('jabatan jb', 'pp.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pp.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pp.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$data['peraturan'] = $this->db->get()->result();
		$data['page'] = 'peraturan_pelaksanaan/peraturan_pelaksanaan';
		$this->load->view('dashboard',$data);
	}
	public function halaman($tahun)
	{
		$data['tahun'] = $tahun;
		$data['hal1'] = 'Pejabat Fungsional Peraturan Pelaksanaan';
		$data['hal2'] = '<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">Peraturan Pelaksanaan</li>';
		$data['instansi'] = $this->db->get('instansi')->result();
		$data['jabatan'] = $this->db->get('jabatan')->result();
		$this->db->select('*');
		$this->db->from('peraturan_pelaksanaan pp');
		$this->db->join('jabatan jb', 'pp.id_jabatan = jb.id_jabatan');
		$this->db->join('instansi ins', 'pp.id_instansi = ins.id_instansi');
		$this->db->where('tahun',$data['tahun']);
		$hak = $this->session->userdata('hak_akses');
 		if ($hak != "admin")
 		{
			$this->db->where('pp.id_instansi',$this->session->userdata('nama_kementrian'));
 		}
		$data['peraturan'] = $this->db->get()->result();
		$data['page'] = 'peraturan_pelaksanaan/peraturan_pelaksanaan';
		$this->load->view('dashboard',$data);
	}

	public function tambah_pejafung_pp()
	{
		$tahun = $this->input->post('tahun');
		$cek_jafung = $this->db->get_where('peraturan_pelaksanaan',array('tahun'=>$tahun))->num_rows();
		if ($cek_jafung==0)
		{
			$jafung = $this->db->get_where('peraturan_pelaksanaan',array('tahun'=>date('Y')))->result();
			// print_r($jafung);die();
			foreach ($jafung as $key => $jf) 
			{
				$data = array(
					'id_jabatan' => $jf->id_jabatan,
					'id_instansi' => $jf->id_instansi,
					'tahun' => $tahun
				);
				$this->db->insert('peraturan_pelaksanaan',$data);
			}
			$this->session->set_flashdata('alert','<script>swal("Berhasil Menambahkan Data Pejafung!", "Klik OK untuk menutup alert!!", "success");</script>');
			redirect('Peraturan_pelaksanaan');
		}
		else
		{
			$this->session->set_flashdata('alert','<script>swal("Gagal Menambahkan Data Pejafung!", "Klik OK untuk menutup alert!!", "error");</script>');
			redirect('Peraturan_pelaksanaan');
		}
	}

	public function update_dok_pejafung()
	{	
		$b = array();
		$tahun = $this->input->post('tahun');
		$dokumen['nama_dok'] = $this->input->post('nama_dok');
		$dokumen['nomor'] = $this->input->post('nomor');
		$dokumen['tahun'] = $this->input->post('tahun');
		$dok1 = $this->input->post('dok_');
		$dok2 = $this->input->post('dok');
		if (empty($dokumen['nama_dok']) AND empty($dok1) AND empty($dok2) AND empty($dokumen['nomor']) AND empty($dokumen['tahun']))
		{
			// die();
			$id=$this->input->post('id');
	    $this->db->update('peraturan_pelaksanaan',array($this->input->post('kk')=>$dokumen['nama_dok']),array('id_peraturan'=>$id));
			$this->session->set_flashdata('alert','<script>swal("Anda Telah Mengosongkan Data!", "Klik OK untuk menutup alert!!", "warning");</script>');
			redirect('Peraturan_pelaksanaan');
		}
		array_push($b,$dokumen);
		// print_r(count($dok2));die();

		for ($i=1;$i<6;$i++)
		{
			$dok_ = 'dok_'.$i;
			$ket_['dok'] = $this->input->post($dok_);
			if($ket_['dok']== null)
			{
	    }
	    else
	    {
	    	array_push($b,$ket_);
	    }
		}

		for ($i=1;$i<6;$i++)
		{
			$dok = 'dok'.$i;
				$config['upload_path']          = './assets/file_dokumen';
		    $config['allowed_types']        = 'jpg|jpeg|png|gif|mp4|3gp|avi|flv|doc|docx|odt|xls|xlsx|csv|sql|ppt|pptx|txt|zip|rar|pdf|7z|mp3|psd|cdr';
		    $config['max_size']             = 999999999999999;
	   		$this->load->library('upload',$config);  
	   		$this->upload->initialize($config);
	    if ( ! $this->upload->do_upload($dok))
	    {
          // $error = array('error' => $this->upload->display_errors());
          // // print_r($error);die();
          // $this->load->view('upload_form', $error);
	    }
	    else
	    {
	            $data = array('dok' => $this->upload->data());
	            $ket['dok'] = $data['dok']['file_name'];
	            // print_r($ket);die();
	            array_push($b,$ket);
	    }
		}

		$a = json_encode($b);
		// print_r($a);die();
		$id=$this->input->post('id');
    $this->db->update('peraturan_pelaksanaan',array($this->input->post('kk')=>$a),array('id_peraturan'=>$id));
		$this->session->set_flashdata('alert','<script>swal("Berhasil Mengupdate Data Pejafung!", "Klik OK untuk menutup alert!!", "success");</script>');
    redirect('Peraturan_pelaksanaan/halaman/'.$tahun);
	}
	public function edit_pejafung_pp($id,$tahun,$kk)
	{
		$data['id'] = $id;
		$data['kk'] = $kk;
		$data['tahun'] = $tahun;
		$data['edit_pp'] = $this->db->get_where('peraturan_pelaksanaan',array('id_peraturan'=>$id))->row();
		$this->load->view('peraturan_pelaksanaan/edit_pejafung_pp',$data);
	}
}
?>