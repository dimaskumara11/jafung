<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('nama');
		$pass = $this->session->userdata('password');
 		if ($user != "admin" AND $pass != "admin")
 		{
 			redirect('Login');
 		}
	}
	public function index()
	{
		$this->load->view('halaman_pengguna');
	}
}
