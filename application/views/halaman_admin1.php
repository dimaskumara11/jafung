<center><div class="col-md-12" id="container1" style="height: auto;"></div></center>
<?php
    $ahli = array();
    $madya = array();
    $muda = array();
    $pertama = array();
    $keterampilan = array();
    $jabatan = array();
    $key = 0;
    $i = 0;
    foreach ($pejafung as $key1 => $tr)
    {
        $html1_ = 0;
        $html2_ = 0;
        $html3_ = 0;
        $html4_ = 0;
        $html9_ = 0;
        $this->db->select('*');
        $this->db->join('jabatan','jabatan.id_jabatan = pejafung.id_jabatan');
        $query = $this->db->get_where('pejafung',array('pejafung.id_jabatan'=>$tr->id_jabatan))->result();
        foreach ($query as $key => $query) 
        {
            $html1 = json_decode($query->ahli_utama,TRUE);
            $pa1 = intval($html1[$i]['pusatatas50']);
            $pb1 = intval($html1[$i]['pusatbawah50']);
            $va1 = intval($html1[$i]['provatas50']);
            $vb1 = intval($html1[$i]['provbawah50']);
            $ka1 = intval($html1[$i]['kabatas50']);
            $kb1 = intval($html1[$i]['kabbawah50']);
            $html1_ += $pa1+$pb1+$va1+$vb1+$ka1+$kb1;

            $html2 = json_decode($query->ahli_madya,TRUE);
            $pa2 = intval($html2[$i]['pusatatas50']);
            $pb2 = intval($html2[$i]['pusatbawah50']);
            $va2 = intval($html2[$i]['provatas50']);
            $vb2 = intval($html2[$i]['provbawah50']);
            $ka2 = intval($html2[$i]['kabatas50']);
            $kb2 = intval($html2[$i]['kabbawah50']);
            $html2_ += $pa2+$pb2+$va2+$vb2+$ka2+$kb2;

            $html3 = json_decode($query->ahli_muda,TRUE);
            $pa3 = intval($html3[$i]['pusatatas50']);
            $pb3 = intval($html3[$i]['pusatbawah50']);
            $va3 = intval($html3[$i]['provatas50']);
            $vb3 = intval($html3[$i]['provbawah50']);
            $ka3 = intval($html3[$i]['kabatas50']);
            $kb3 = intval($html3[$i]['kabbawah50']);
            $html3_ += $pa3+$pb3+$va3+$vb3+$ka3+$kb3;

            $html4 = json_decode($query->ahli_pertama,TRUE);
            $pa4 = intval($html4[$i]['pusatatas50']);
            $pb4 = intval($html4[$i]['pusatbawah50']);
            $va4 = intval($html4[$i]['provatas50']);
            $vb4 = intval($html4[$i]['provbawah50']);
            $ka4 = intval($html4[$i]['kabatas50']);
            $kb4 = intval($html4[$i]['kabbawah50']);
            $html4_ += $pa4+$pb4+$va4+$vb4+$ka4+$kb4;

            $html5 = json_decode($query->penyelia,TRUE);
            $pa5 = intval($html5[$i]['pusatatas50']);
            $pb5 = intval($html5[$i]['pusatbawah50']);
            $va5 = intval($html5[$i]['provatas50']);
            $vb5 = intval($html5[$i]['provbawah50']);
            $ka5 = intval($html5[$i]['kabatas50']);
            $kb5 = intval($html5[$i]['kabbawah50']);
            $html5_ = $pa5+$pb5+$va5+$vb5+$ka5+$kb5;

            $html6 = json_decode($query->pelaksana_lanjutan,TRUE);
            $pa6 = intval($html6[$i]['pusatatas50']);
            $pb6 = intval($html6[$i]['pusatbawah50']);
            $va6 = intval($html6[$i]['provatas50']);
            $vb6 = intval($html6[$i]['provbawah50']);
            $ka6 = intval($html6[$i]['kabatas50']);
            $kb6 = intval($html6[$i]['kabbawah50']);
            $html6_ = $pa6+$pb6+$va6+$vb6+$ka6+$kb6;

            $html7 = json_decode($query->terampil,TRUE);
            $pa7 = intval($html7[$i]['pusatatas50']);
            $pb7 = intval($html7[$i]['pusatbawah50']);
            $va7 = intval($html7[$i]['provatas50']);
            $vb7 = intval($html7[$i]['provbawah50']);
            $ka7 = intval($html7[$i]['kabatas50']);
            $kb7 = intval($html7[$i]['kabbawah50']);
            $html7_ = $pa7+$pb7+$va7+$vb7+$ka7+$kb7;

            $html8 = json_decode($query->pelaksana_pemula,TRUE);
            $pa8 = intval($html8[$i]['pusatatas50']);
            $pb8 = intval($html8[$i]['pusatbawah50']);
            $va8 = intval($html8[$i]['provatas50']);
            $vb8 = intval($html8[$i]['provbawah50']);
            $ka8 = intval($html8[$i]['kabatas50']);
            $kb8 = intval($html8[$i]['kabbawah50']);
            $html8_ = $pa8+$pb8+$va8+$vb8+$ka8+$kb8;

            $html9_ += $html5_+$html6_+$html7_+$html8_;
        }
        array_push($madya, $html2_);
        array_push($ahli, $html1_);
        array_push($muda, $html3_);
        array_push($pertama, $html4_);
        array_push($keterampilan, $html9_);
        array_push($jabatan, $query->nama_jabatan);
    } 
?>
<script type="text/javascript">

Highcharts.chart('container1', {
    chart: {
        type: 'column',
        plotShadow: true,
    },
    title: {
        text: '<b>Grafik Pejafung Aktif Per Jenjang Jabatan</b>'
    },
    xAxis: {
        categories: <?=json_encode($jabatan);?>,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Pejafung'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.f} Pejafung</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Utama',
        data: <?php print_r(json_encode($ahli))?>

    },{
        name: 'Madya',
        data: <?php print_r(json_encode($madya))?>

    },{
        name: 'Muda',
        data: <?php print_r(json_encode($muda))?>

    },{
        name: 'Pertama',
        data: <?php print_r(json_encode($pertama))?>

    },{
        name: 'Keterampilan',
        data: <?php print_r(json_encode($keterampilan))?>

    }]
});
</script>