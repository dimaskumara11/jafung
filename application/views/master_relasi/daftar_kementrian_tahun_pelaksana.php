<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="overflow: hidden" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judul1">Copy Detail Jafung</h3>
      </div>
      <div class="modal-body">
        <form action="<?=base_url()?>Master_relasi_pelaksana/copy_jafung" method="post">
          <input type="hidden" name="id_instansi" value="<?=$instansi?>">
          <input type="hidden" name="tahun_" value="">
          <a style="color: red">* Data yang di copy tidak termasuk urusan</a>
          <select class="form-control" name="tahun">
            <?php
            for($i=2016;$i<=2031;$i++)
            {?>
            <option value="<?=$i?>"><?=$i?></option>
            <? }?>
          </select><br>
          <button class="btn btn-success btn-xs">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="overflow: hidden" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judul3">Tambah Jafung</h3>
      </div>
      <div class="modal-body">
        <form action="<?=base_url()?>Master_relasi_pelaksana/tambah_jafung" method="post">
          <input type="hidden" name="id_instansi" value="<?=$instansi?>">
          <input type="hidden" name="tahun">
          <div class="form-group">
            <label>Nama Jabatan</label>
            <select class="form-control" name="nama_jabatan[]" required="">
              <option value="0">Pilih Jabatan</option>
              <?php foreach ($jabatan as $key => $jb) 
              {?>
                <option value="<?=$jb->id_jabatan?>"><?=$jb->nama_jabatan?></option>
              <?php } ?>
            </select>
            <label>Urusan</label>
            <textarea class="form-control" name="urusan[]" value="" required=""></textarea><br>
            <div class="inputjabatan1">
              
            </div>
            <button type="button" class="btn btn-success btn-sm btn-block tambahfield"><i class="fa fa-plus"></i></button><br>
          </div>
          <button class="btn btn-primary btn-sm">Tambah</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judul2">Detail Jafung</h3>
      </div>
      <div class="modal-body">
        <div class="detailrelasi"></div>
      </div>
    </div>
  </div>
</div>
<div class="content">
<input type="hidden" name="password_alumni" value="<?=$this->session->userdata('password')?>">
<a href="<?=base_url()?>Master_relasi_pelaksana/" style="color: red"><button class="btn btn-sm btn-danger">Kembali</button></a>
<h3 align="center"><b>Daftar Jafung <?=$hal3?></b></h3>
<input type="hidden" class="btn btn-primary edit_alumni_modal1" data-toggle="modal" data-target="#exampleModal1">
	<div class="panel panel-primary">
		<div class="panel panel-body table-responsive">
			<table class="table table-bordered table-striped">
        <thead class="btn-primary">
          <tr>
          	<th>No</th>
            <th>Tahun</th>
            <th>Jumlah Jafung</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $i = 1;
           foreach ($relasi as $key => $rl) 
          {?>
          <tr>
            <td width="10px"><?=$i++?></td>
            <td><?=$rl->tahun?></td>
            <td>
            <?php
            if ($this->session->userdata('hak_akses') != "admin")
            {
              $query1 = $this->db->query('SELECT count("id_instansi") as total_instansi from relasi_pelaksana where tahun = '.$rl->tahun.' AND id_instansi = '.$this->session->userdata("nama_kementrian").'')->row();
              echo $query1->total_instansi;
            }
            else 
            {
              $query1 = $this->db->query('SELECT count("id_instansi") as total_instansi from relasi_pelaksana where tahun = '.$rl->tahun.' AND id_instansi = '.$rl->id_instansi.'')->row();
              echo $query1->total_instansi;
            }
            ?></td>
            <td style="width: 120px">
              <button class="btn btn-primary btn-xs detail" id="<?=$rl->tahun?>" link="<?=$rl->id_instansi?>">Detail</button>
              <button class="btn btn-success btn-xs copy" id="<?=$rl->tahun?>" data-toggle="modal" data-target="#exampleModal3">Copy</button>
            </td>
          </tr>
          <?php }?>
        </tbody>
      </table>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.tambahfield').on('click',function()
    {
      $.ajax({
       method : 'get',
       url : '<?=base_url()?>Master_relasi_pelaksana/tampiljabatan/',
       dataType:'json',
       success : function(data)
       {
          var html = '';
          for(i=0;i<data.length;i++)
          {
            html += '<option value="'+data[i].id_jabatan+'">'+data[i].nama_jabatan+'</option>';
          }
          var html1 = '<select class="form-control" name="nama_jabatan[]" class="inputjabatan">'+
                      '<option value="0">Pilih Jabatan</option>'+
                        html+
                      '</select>'+
                      '<label>Urusan</label>'+
                      '<textarea class="form-control" name="urusan[]" value="" required=""></textarea><br>';
      $('.inputjabatan1').append(html1);
       }
      })
    })

    $('.detail').on('click',function()
    {
      var tahun = $(this).attr('id');
      var id = $(this).attr('link');
      $.ajax({
       method : 'post',
       url : '<?=base_url()?>Master_relasi_pelaksana/detail_relasi_tahun/'+id+'/'+tahun,
       success : function(data)
       {
          $('.judul2').html('Detail Jafung Tahun '+tahun);
          $('.edit_alumni_modal1').click();
          $('.detailrelasi').html(data);
       }
      })
    })
    $('.copy').on('click',function()
    {
      var a = $(this).attr('id');
      $('.judul1').html('Copy Data Jafung Tahun '+a);
      $('[name="tahun_"]').val(a);
      $('[name="tahun"]').val('2018');
    })
    $('.detailrelasi').on('click','.tamabh_jafung',function()
    {
      var a = $(this).attr('link');
      $('.judul3').html('Tambah Jafung Tahun '+a);
      $('[name="tahun"]').val(a);
      $('#exampleModal1').fadeOut();
    })
    $('.detailrelasi').on('click','.hapus_jafung',function()
    {
      var id = $(this).attr('id');
      var a = $(this).attr('link');

      $.ajax({
       method : 'post',
       url : '<?=base_url()?>Master_relasi_pelaksana/edit_relasi_tahun/'+id+'/'+a,
       success : function(data)
       {
          // $('.edit_alumni_modal1').click();
          $('.judul2').html('Edit Jafung Tahun '+a);
          $('.detailrelasi').html('').slideUp();
          $('.detailrelasi').html(data).slideDown();
          $('[name="tahun1"]').val(a);
          $('[name="id1"]').val(id);
       }
      })
    })
    $('.detailrelasi').on('click','.hapus_jafung',function()
    {
      
    })
  })
</script>