<div class="modal fade " id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title">Tambah Relasi Pelaksana</h3>
      </div>
      <div class="modal-body">
        <form id="formtambah" action="<?=base_url()?>Master_relasi_pelaksana/tambah_relasi" method="post">
          <input name="id_relasi" type="hidden">
          <div class="form-group">
            <label>Nama Kementerian</label>
            <select class="form-control kmt" name="nama_kementrian" required="">
              <option value="0">Pilih Kementerian</option>
              <?php foreach ($instansi as $key => $ins) 
              {?>
                <option value="<?=$ins->id_instansi?>"><?=$ins->nama_instansi?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Nama Jabatan</label>
            <select class="form-control jbt" name="nama_jabatan[]" required="">
              <option value="0">Pilih Jabatan</option>
              <?php foreach ($jabatan as $key => $jb) 
              {?>
                <option value="<?=$jb->id_jabatan?>"><?=$jb->nama_jabatan?></option>
              <?php } ?>
            </select>
            <label>Urusan</label>
            <textarea class="form-control" name="urusan[]" value="" required=""></textarea><br>
            <div class="inputjabatan1">
              
            </div>
            <button type="button" class="btn btn-success btn-xs btn-block tambahfield"><i class="fa fa-plus"></i></button><br>
          </div>
          <div class="form-group">
            <label>Tahun</label>
            <select class="form-control" name="tahun">
              <?php
              for($i=2016;$i<=2031;$i++)
              {?>
              <option value="<?=$i?>"><?=$i?></option>
              <? }?>
            </select>
          </div>
          <button type="button" class="btn btn-primary tambah"><i class="glyphicon glyphicon-floppy-save"></i> Simpan</button>
          <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <input type="hidden" name="pass" value="<?=$this->session->userdata('password')?>">
	<h3 align="center"><b>Daftar Relasi Pelaksana</b></h3>
<button type="button" class="btn btn-primary edit_alumni_modal1" data-toggle="modal" data-target="#exampleModal1"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Relasi Pelaksana</button><br><br>
	<div class="panel panel-primary">
		<div class="panel panel-body table-responsive">
			<table class="table table-bordered table-striped">
        <thead class="btn-primary">
          <tr>
          	<th width="10px">No</th>
            <th>Nama Kementerian</th>
            <th>Option</th>
          </tr>
        </thead>
        <!-- <tbody id="show_data"> -->
        <tbody>
          <?php
          if(empty($relasi))
          {
            echo '<tr><th colspan="3"><center><br>Data Kosong</center></th></tr>';
          }
          else
          {
            $i = 1;
             foreach ($relasi as $key => $rl) 
            {?>
            <tr>
              <td><?=$i++?></td>
              <td><?=$rl->nama_instansi?></td>
              <td style="width: 120px">
                <form action="" class="formrelasi" method="post">
                  <input type="hidden" name="id_instansi" value="<?=$rl->id_instansi?>">
                  <input type="hidden" name="nama_kementrian" value="<?=$rl->nama_instansi?>">
                  <button class="btn btn-warning btn-xs detail">Detail</button>
                  <button type="button" id="<?=$rl->id_instansi?>" class="btn btn-danger btn-xs delete">Delete</button>
                </form>
              </td>
            </tr>
          <? }
           }?>
        </tbody>
      </table>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function()
  {
    $('.tambahfield').on('click',function()
    {
      $.ajax({
       method : 'get',
       url : '<?=base_url()?>Master_relasi_pelaksana/tampiljabatan/',
       dataType:'json',
       success : function(data)
       {
          var html = '';
          for(i=0;i<data.length;i++)
          {
            html += '<option value="'+data[i].id_jabatan+'">'+data[i].nama_jabatan+'</option>';
          }
          var html1 = '<label>Nama Jabatan</label>'+
                      '<select class="form-control" name="nama_jabatan[]" class="inputjabatan">'+
                      '<option value="0">Pilih Jabatan</option>'+
                        html+
                      '</select>'+
                      '<label>Urusan</label>'+
                      '<textarea class="form-control" name="urusan[]" value="" required=""></textarea><br>'+;
      $('.inputjabatan1').append(html1);
       }
      })
    })

    $('.edit_alumni_modal1').on('click',function()
    {
      $('[name="nama_kementrian"]').val('0');
      $('[name="nama_jabatan"]').val('0');
    })
    $('.detail').on('click',function()
    {
      $('.formrelasi').attr('action','<?=base_url()?>Master_relasi_pelaksana/detail_relasi/');
      $('.formrelasi').submit();
    })

    $('.tambah').on('click',function()
    {
      if ($('.kmt').val() == 0)
      {          
        swal({
            title : "Harap Isi Nama Kementerian !!",
            icon: "error",
          });die();
      }
      else if ($('.jbt').val() == 0)
      {          
        swal({
            title : "Harap Isi Nama Jabatan !!",
            icon: "error",
          });die();
      }
      else if ($('[name="tahun"]').val() == 0)
      {          
        swal({
            title : "Harap Isi Tahun !!",
            icon: "error",
          });die();
      }
      $('#formtambah').submit();
    })

    $('.delete').on('click',function()
    {
      var id = $('[name="pass"]').val();
      var id1 = $(this).attr('id');
      swal("Anda Yakin Ingin Menghapus Data Ini ?","", {
        icon : 'warning',
      content: {
        element: "input",
        attributes: {
          placeholder: "Masukkan Password",
          type: "password",
        },
      },
      })
        .then((value) => {
        if (`${value}` == id)
        {
            delete_relasi(id1);
        }
        else
        {
            swal("Password Salah !!!","","error");
        }
        });
    })
    function delete_relasi(id)
    {
      $.ajax({
       method : 'get',
       url : '<?=base_url()?>Master_relasi_pelaksana/delete_relasi/'+id,
       success : function(data)
       {
          swal({
            title : "Hapus Data Berhasil !!",
            icon: "success",
          });
          location.reload(true);
       }
      })
    }
  })
</script>