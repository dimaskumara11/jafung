<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
      	<div class="pull-left"></div>
      	<div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judulmodal"></h3>
      </div>
      <div class="modal-body">
      	<div class="edit_pj"></div>
      </div>
    </div>
  </div>
</div>
<br>
<div style="width: 300px" class="pull-right">
<form action="<?=base_url()?>Proyeksi/" method="post">
<input type="hidden" name="pejaf" value="<?=$title1?>">
<div class="col-md-8">
	<select name="tahun" class="form-control" required="">
	<?php
	for($i=2018;$i<2031;$i++)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	?>
	</select>
</div>
<div class="col-md-1"><button type="submit" class="btn btn-success"><b>Jelajahi</b></button></div>
</form>
<input type="hidden" name="tahun__" value="<?=$tahun?>">
<br><br>
</div>
<!-- <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal1"><b><i class="fa fa-plus"></i> Tambah Tahun</b></button> -->
<br>
<br>
<div class="panel panel-primary">
	<div class="panel panel-heading">
		<h3 align="center">PROYEKSI PEJABAT FUNGSIONAL PROYEKSI TAHUN <?=$tahun?></h3>
	</div>
	<div class="panel-body table-responsive">
		<table class="table table-hover table-bordered">
			<thead style="font-weight: bold;text-align: center;">
				<tr class="btn-success">
					<td rowspan="4">No</td>
					<td rowspan="4">Instansi Pembina</td>
					<td rowspan="4">Nama Jabatan</td>
					<td rowspan="4">No Permai</td>
					<td colspan="8">Jumlah Pejabat Fungsional Saat ini Sampai Desember</td>
					<td rowspan="4" class="btn-warning">Jumlah</td>
				</tr>
				<tr class="btn-success">
					<td colspan="4">Keahlian</td>
					<td colspan="4">Keterampilan</td>
				</tr>
				<tr class="btn-primary">
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
				</tr>
				<tr class="btn-primary">
					<td>Utama/Ahli Utama</td>
					<td>Madya/Ahli Madya</td>
					<td>Muda/Ahli Muda</td>
					<td>Pertama/Ahli Pertama</td>
					<td>Penyelia</td>
					<td>Mahir/Pelaksana Lanjutan</td>
					<td>Terampil/Pelaksana</td>
					<td>Pelaksana Pemula/Pelaksana</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(empty($pejafung))
				{
					echo '<tr><th colspan="2"><br><center>Data Kosong</center></th></tr>';
				}
				else
				{
				$jk = 1;
				$au_ = 0;
				$amd_ = 0;
				$amu_ = 0;
				$apt_ = 0;
				$apy_ = 0;
				$apl_ = 0;
				$te_ = 0;
				$pp_ = 0;
				$total_ = 0;
				// print_r($pejafung);die();
				foreach ($pejafung as $key => $pj) 
				{?>
					<tr>
						<td><b><?=$jk++?></b></td>
						<td><?=$pj->nama_instansi?></td>
						<td><?=$pj->nama_jabatan?></td>
						<td align="center"><?=$pj->no_permai?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_ahli_utama" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
							<? $au_ += $pj->ahli_utama?>
							<?=$pj->ahli_utama?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_ahli_madya" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
							<? $amd_ += $pj->ahli_madya?>
							<?=$pj->ahli_madya?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_ahli_muda" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
							<? $amu_ += $pj->ahli_muda?>
							<?=$pj->ahli_muda?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_ahli_pertama" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
							<? $apt_ += $pj->ahli_pertama?>
							<?=$pj->ahli_pertama?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_penyelia" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
							<? $apy_ += $pj->penyelia?>
							<?=$pj->penyelia?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_pelaksana_lanjutan" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
							<? $apl_ += $pj->pelaksana_lanjutan?>
							<?=$pj->pelaksana_lanjutan?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_terampil" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
							<? $te_ += $pj->terampil?>
							<?=$pj->terampil?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_pelaksana_pemula" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
							<? $pp_ += $pj->pelaksana_pemula?>
							<?=$pj->pelaksana_pemula?></td>
						<td id="<?=$pj->id_pejafung?>" align="center" style=" font-size: 18px;"><b>
							<?php 
								$total = $pj->ahli_utama+$pj->ahli_madya+$pj->ahli_muda+$pj->ahli_pertama+$pj->penyelia+$pj->pelaksana_lanjutan+$pj->terampil+$pj->pelaksana_pemula; 
								$total_ += $total ;
								echo $total;
							?></b></td>
					</tr>

				<?php	}?>
				<tr align="center" style="font-weight: bold; font-size: 18px;">
					<td colspan="4">Total</td>
					<td><?=$au_?></td>
					<td><?=$amd_?></td>
					<td><?=$amu_?></td>
					<td><?=$apt_?></td>
					<td><?=$apy_?></td>
					<td><?=$apl_?></td>
					<td><?=$te_?></td>
					<td><?=$pp_?></td>
					<td><?=$total_?></td>
				</tr>
			<? }?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
		$('.edit_ahli_utama').on('click',function()
		{
			var id = $(this).attr('id');
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Proyeksi/edit_pejafung/'+id+'/ahli_utama/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Ahli Utama');
				}
			})
		})
		$('.edit_ahli_madya').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Proyeksi/edit_pejafung/'+id+'/ahli_madya/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Ahli Madya');
				}
			})
		})
		$('.edit_ahli_muda').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Proyeksi/edit_pejafung/'+id+'/ahli_muda/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Ahli Muda');
				}
			})
		})
		$('.edit_ahli_pertama').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Proyeksi/edit_pejafung/'+id+'/ahli_pertama/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Ahli Pertama');
				}
			})
		})
		$('.edit_penyelia').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Proyeksi/edit_pejafung/'+id+'/penyelia/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Penyelia');
				}
			})
		})
		$('.edit_pelaksana_lanjutan').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Proyeksi/edit_pejafung/'+id+'/pelaksana_lanjutan/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Pelaksana Lanjutan');
				}
			})
		})
		$('.edit_terampil').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Proyeksi/edit_pejafung/'+id+'/terampil/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Terampil');
				}
			})
		})
		$('.edit_pelaksana_pemula').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Proyeksi/edit_pejafung/'+id+'/pelaksana_pemula/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Pelaksana Pemula');
				}
			})
		})
	})
</script>