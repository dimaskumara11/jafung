<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
      	<div class="pull-left"></div>
      	<div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judulmodal"></h3>
      </div>
      <div class="modal-body">
      	<div class="edit_pj"></div>
      </div>
    </div>
  </div>
</div>
<div style="width: 300px" class="pull-right">
<form action="<?=base_url()?>Pejafung/" method="post">
<input type="hidden" name="pejaf" value="<?=$title1?>">
<div class="col-md-8">
	<select name="tahun" class="form-control" required="">
	<?php
	for($i=2018;$i<2031;$i++)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	?>
	</select>
</div>
<div class="col-md-1"><button type="submit" class="btn btn-success"><b>Jelajahi</b></button></div>
</form>
<input type="hidden" name="tahun__" value="<?=$tahun?>">
<br><br>
</div>
<!-- <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal1"><b><i class="fa fa-plus"></i> Tambah Tahun</b></button> -->
<br>
<br>
<div class="panel panel-primary">
	<div class="panel panel-heading">
	<?php
		if($title1 == "PROYEKSI")
		{
			echo '<h3 align="center">PROYEKSI PEJABAT FUNGSIONAL <?=$title1?> TAHUN '.$tahun.'</h3>';
		}
		else
		{
			echo '<h3 align="center">PEJABAT FUNGSIONAL AKTIF TAHUN '.$tahun.'</h3>';
		}
	?>
	</div>
	<div class="panel-body table-responsive">
		<table class="table table-hover table-bordered">
			<thead style="font-weight: bold;text-align: center;">
				<tr class="btn-success">
					<td rowspan="4">No</td>
					<td rowspan="4">Instansi Pembina</td>
					<td rowspan="4">Nama Jabatan</td>
					<td rowspan="4">No Permai</td>
	<?php
		if($title1 == "PROYEKSI")
		{
			echo '<td colspan="8">Proyeksi Kebutuhan Pejabat Fungsional</td>';
		}
		else
		{
			echo '<td colspan="8">Jumlah Pejabat Fungsional Saat ini Sampai Desember</td>';
		}
	?>
					<td rowspan="4" class="btn-warning">Jumlah</td>
				</tr>
				<tr class="btn-success">
					<td colspan="4">Keahlian</td>
					<td colspan="4">Keterampilan</td>
				</tr>
				<tr class="btn-primary">
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
				</tr>
				<tr class="btn-primary">
					<td>Utama/Ahli Utama</td>
					<td>Madya/Ahli Madya</td>
					<td>Muda/Ahli Muda</td>
					<td>Pertama/Ahli Pertama</td>
					<td>Penyelia</td>
					<td>Mahir/Pelaksana Lanjutan</td>
					<td>Terampil/Pelaksana</td>
					<td>Pelaksana Pemula/Pelaksana</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				// print_r($pejafung);
				if(empty($pejafung))
				{
					echo '<tr><th colspan="2"><br><center>Data Kosong</center></th></tr>';
				}
				else
				{
				$jk = 1;
				$au_ = 0;
				$amd_ = 0;
				$amu_ = 0;
				$apt_ = 0;
				$apy_ = 0;
				$apl_ = 0;
				$te_ = 0;
				$pp_ = 0;
				$total_ = 0;
				// print_r($pejafung);die();
				foreach ($pejafung as $key => $pj) 
				{?>
					<tr>
						<td><b><?=$jk++?></b></td>
						<td><?=$pj->nama_instansi?></td>
						<td><?=$pj->nama_jabatan?></td>
						<td align="center"><?=$pj->no_permai?></td>
						<td id="<?=$pj->id_pejafung?>" class="edit_ahli_utama" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
<?php
$hs = json_decode($pj->ahli_utama,TRUE);
// print_r($hs);
$i=0;
	$au1 = intval($hs[$i]['pusatatas50'])+intval($hs[$i]['pusatbawah50']);
	$au2 = intval($hs[$i]['provatas50'])+intval($hs[$i]['provbawah50']);
	$au3 = intval($hs[$i]['kabatas50'])+intval($hs[$i]['kabbawah50']);
	$au = $au1+$au2+$au3;
	$au_ += $au;
	echo $au;
?>
						</td>
						<td id="<?=$pj->id_pejafung?>" class="edit_ahli_madya" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
<?php
$hs = json_decode($pj->ahli_madya,TRUE);
$i=0;
	$amd1 = intval($hs[$i]['pusatatas50'])+intval($hs[$i]['pusatbawah50']);
	$amd2 = intval($hs[$i]['provatas50'])+intval($hs[$i]['provbawah50']);
	$amd3 = intval($hs[$i]['kabatas50'])+intval($hs[$i]['kabbawah50']);
	$amd = $amd1+$amd2+$amd3;
	$amd_ += $amd;
	echo $amd;
?>
						</td>
						<td id="<?=$pj->id_pejafung?>" class="edit_ahli_muda" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
<?php
$hs = json_decode($pj->ahli_muda,TRUE);
$i=0;
	$amu1 = intval($hs[$i]['pusatatas50'])+intval($hs[$i]['pusatbawah50']);
	$amu2 = intval($hs[$i]['provatas50'])+intval($hs[$i]['provbawah50']);
	$amu3 = intval($hs[$i]['kabatas50'])+intval($hs[$i]['kabbawah50']);
	$amu = $amu1+$amu2+$amu3;
	$amu_ += $amu;
	echo $amu;
?>
						</td>
						<td id="<?=$pj->id_pejafung?>" class="edit_ahli_pertama" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
<?php
$hs = json_decode($pj->ahli_pertama,TRUE);
$i=0;
	$apt1 = intval($hs[$i]['pusatatas50'])+intval($hs[$i]['pusatbawah50']);
	$apt2 = intval($hs[$i]['provatas50'])+intval($hs[$i]['provbawah50']);
	$apt3 = intval($hs[$i]['kabatas50'])+intval($hs[$i]['kabbawah50']);
	$apt = $apt1+$apt2+$apt3;
	$apt_ += $apt;
	echo $apt;
?>
						</td>
						<td id="<?=$pj->id_pejafung?>" class="edit_penyelia" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
<?php
$hs = json_decode($pj->penyelia,TRUE);
$i=0;
	$apy1 = intval($hs[$i]['pusatatas50'])+intval($hs[$i]['pusatbawah50']);
	$apy2 = intval($hs[$i]['provatas50'])+intval($hs[$i]['provbawah50']);
	$apy3 = intval($hs[$i]['kabatas50'])+intval($hs[$i]['kabbawah50']);
	$apy = $apy1+$apy2+$apy3;
	$apy_ += $apy;
	echo $apy;
?>
						</td>
						<td id="<?=$pj->id_pejafung?>" class="edit_pelaksana_lanjutan" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
<?php
$hs = json_decode($pj->pelaksana_lanjutan,TRUE);
$i=0;
	$apl1 = intval($hs[$i]['pusatatas50'])+intval($hs[$i]['pusatbawah50']);
	$apl2 = intval($hs[$i]['provatas50'])+intval($hs[$i]['provbawah50']);
	$apl3 = intval($hs[$i]['kabatas50'])+intval($hs[$i]['kabbawah50']);
	$apl = $apl1+$apl2+$apl3;
	$apl_ += $apl;
	echo $apl;
?>
						</td>
						<td id="<?=$pj->id_pejafung?>" class="edit_terampil" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
<?php
$hs = json_decode($pj->terampil,TRUE);
$i=0;
	$te1 = intval($hs[$i]['pusatatas50'])+intval($hs[$i]['pusatbawah50']);
	$te2 = intval($hs[$i]['provatas50'])+intval($hs[$i]['provbawah50']);
	$te3 = intval($hs[$i]['kabatas50'])+intval($hs[$i]['kabbawah50']);
	$te = $te1+$te2+$te3;
	$te_ += $te;
	echo $te;
?>
						</td>
						<td id="<?=$pj->id_pejafung?>" class="edit_pelaksana_pemula" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer" align="center">
<?php
$hs = json_decode($pj->pelaksana_pemula,TRUE);
$i=0;
	$pp1 = intval($hs[$i]['pusatatas50'])+intval($hs[$i]['pusatbawah50']);
	$pp2 = intval($hs[$i]['provatas50'])+intval($hs[$i]['provbawah50']);
	$pp3 = intval($hs[$i]['kabatas50'])+intval($hs[$i]['kabbawah50']);
	$pp = $pp1+$pp2+$pp3;
	$pp_ += $pp;
	echo $pp;
?>
						</td>
						<td id="<?=$pj->id_pejafung?>" align="center" style=" font-size: 18px;"><b>
							<?php 
								$total = $au+$amd+$amu+$apt+$apy+$apl+$te+$pp; 
								$total_ += $total ;
								echo $total;
							?></b></td>
					</tr>

				<?php	}?>
				<tr align="center" style="font-weight: bold; font-size: 18px;">
					<td colspan="4">Total</td>
					<td><?=$au_?></td>
					<td><?=$amd_?></td>
					<td><?=$amu_?></td>
					<td><?=$apt_?></td>
					<td><?=$apy_?></td>
					<td><?=$apl_?></td>
					<td><?=$te_?></td>
					<td><?=$pp_?></td>
					<td><?=$total_?></td>
				</tr>
			<?php }?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
		$('.edit_ahli_utama').on('click',function()
		{
			var id = $(this).attr('id');
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Pejafung/edit_pejafung/'+id+'/ahli_utama/'+pj+'/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Ahli Utama');
					$('#formupdate').attr('action','<?=base_url()?>Pejafung/update_pejafung');
				}
			})
		})
		$('.edit_ahli_madya').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Pejafung/edit_pejafung/'+id+'/ahli_madya/'+pj+'/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Ahli Madya');
					$('#formupdate').attr('action','<?=base_url()?>Pejafung/update_pejafung');
				}
			})
		})
		$('.edit_ahli_muda').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Pejafung/edit_pejafung/'+id+'/ahli_muda/'+pj+'/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Ahli Muda');
					$('#formupdate').attr('action','<?=base_url()?>Pejafung/update_pejafung');
				}
			})
		})
		$('.edit_ahli_pertama').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Pejafung/edit_pejafung/'+id+'/ahli_pertama/'+pj+'/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Ahli Pertama');
					$('#formupdate').attr('action','<?=base_url()?>Pejafung/update_pejafung');
				}
			})
		})
		$('.edit_penyelia').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Pejafung/edit_pejafung/'+id+'/penyelia/'+pj+'/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Penyelia');
					$('#formupdate').attr('action','<?=base_url()?>Pejafung/update_pejafung');
				}
			})
		})
		$('.edit_pelaksana_lanjutan').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Pejafung/edit_pejafung/'+id+'/pelaksana_lanjutan/'+pj+'/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Pelaksana Lanjutan');
					$('#formupdate').attr('action','<?=base_url()?>Pejafung/update_pejafung');
				}
			})
		})
		$('.edit_terampil').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Pejafung/edit_pejafung/'+id+'/terampil/'+pj+'/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Terampil');
					$('#formupdate').attr('action','<?=base_url()?>Pejafung/update_pejafung');
				}
			})
		})
		$('.edit_pelaksana_pemula').on('click',function()
		{
			var pj = $('[name="pejaf"]').val()
			var tahun = $('[name="tahun__"]').val();
			var id = $(this).attr('id');
			$.ajax({
				url : '<?= base_url()?>Pejafung/edit_pejafung/'+id+'/pelaksana_pemula/'+pj+'/'+tahun,
				success : function(data)
				{
					$('.edit_pj').html(data);
					$('.judulmodal').html('Form Update Pelaksana Pemula');
					$('#formupdate').attr('action','<?=base_url()?>Pejafung/update_pejafung');
				}
			})
		})
	})
</script>