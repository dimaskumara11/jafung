<form action="<?=base_url()?>Profil_jf/update_profil_jf" method="post" id="formupdate">
	<input type="hidden" name="id_profil_jf" value="<?=$profil->id_profil_jf?>">
  <div class="form-group">
  	<label>Kedudukan</label>
		<select name="kedudukan" class="form-control">
			<option value="">Pilih Kedudukan</option>
			<option value="Pusat">Pusat</option>
			<option value="Daerah">Daerah</option>
		</select>
	</div>
  <div class="form-group">
  	<label>Kompetensi Teknis</label>
		<textarea name="kompetensi_teknis" class="form-control"><?=$profil->kompetensi_teknis?></textarea>
	</div>
  <div class="form-group">
  	<label>Kategori</label><br>
		<input type="checkbox" class="check_keahlian" name="kategori[]" value="keahlian">Keahlian<br>
		<input type="checkbox" class="check_keterampilan" name="kategori[]" value="keterampilan">Keterampilan
	</div>
  <fieldset class="form-group">
  	<div class="keahlian col-md-6">
  		<label>Keahlian</label><br>
	  	<input type="checkbox" name="jenjang_jabatan_keahlian[]" value="Ahli Utama">Ahli Utama<br>
	  	<input type="checkbox" name="jenjang_jabatan_keahlian[]" value="Ahli Madya">Ahli Madya<br>
	  	<input type="checkbox" name="jenjang_jabatan_keahlian[]" value="Ahli Muda">Ahli Muda<br>
	  	<input type="checkbox" name="jenjang_jabatan_keahlian[]" value="Ahli Pertama">Ahli Pertama<hr>
  	</div>
  	<div class="keterampilan col-md-6">
  		<label>Keterampilan</label><br>
	  	<input type="checkbox" name="jenjang_jabatan_keterampilan[]" value="Penyelia">Penyelia<br>
	  	<input type="checkbox" name="jenjang_jabatan_keterampilan[]" value="Pelaksana Lanjutan">Pelaksana Lanjutan<br>
	  	<input type="checkbox" name="jenjang_jabatan_keterampilan[]" value="Pelaksana">Pelaksana<br>
	  	<input type="checkbox" name="jenjang_jabatan_keterampilan[]" value="Pelaksana Pemula">Pelaksana Pemula<hr>
  	</div>
	</fieldset><!-- 
	<div class="form-group">
		<label>Nomor Permenpanrb</label>
		<input type="text" class="form-control" name="permenpanrb">
	</div>
	<div class="form-group">
		<label>Junlak Junkis</label>
		<input type="text" class="form-control" name="junlak_junkis">
	</div>
	<div class="form-group">
		<label>Tugas Pokok</label>
		<input type="text" class="form-control" name="tugas_pokok">
	</div>
	<div class="form-group">
		<label>Perpres Tunjangan</label>
		<input type="text" class="form-control" name="perpres_tunjangan">
	</div>
	<div class="form-group">
		<label>Pengaturan Bup</label>
		<input type="text" class="form-control" name="pengaturan_bup">
	</div>
	<div class="form-group">
		<label>Pengangkatan Pertama</label>
		<input type="file" class="form-control" name="pengangkatan_pertama">
	</div>
	<div class="form-group">
		<label>Pengangkatan Penyesuaian</label>
		<input type="file" class="form-control" name="pengangkatan_penyesuaian">
	</div>
	<div class="form-group">
		<label>Perpindahan Jabatan Lain</label>
		<input type="file" class="form-control" name="perpndahan_jabatan_lain">
	</div>
	<div class="form-group">
		<label>Promosi</label>
		<input type="file" class="form-control" name="promosi">
	</div>
	<div class="form-group">
		<label>Penialaian Kerja</label>
		<input type="file" class="form-control" name="penialaian_kerja">
	</div>
	<div class="form-group">
		<label>Pemberhentian</label>
		<input type="file" class="form-control" name="pemberhentian">
	</div>
	<div class="form-group">
		<label>Pengangkatan Kembali</label>
		<input type="file" class="form-control" name="pengangkatan_kembali">
	</div>
	<div class="form-group">
		<label>Jenjang, Angka, Kredit, Tunjangan Fungsional</label>
		<input type="file" class="form-control" name="jenjang_tunjangan_fungsional">
	</div> -->
  <button type="submit" class="btn btn-primary update btn-sm"><i class="glyphicon glyphicon-ok"></i></button>
  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></button>
</form>