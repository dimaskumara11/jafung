<input type="hidden" class="btn btn-primary edit_alumni_modal" data-toggle="modal" data-target="#exampleModal">
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title">Edit Nama Dan Kontak</h3>
      </div>
      <div class="modal-body">
        <form id="formtambah" action="<?= base_url()?>Profil/update_profil" method="post">
          <div class="form-group">
            <label>Nama Pengguna</label>
            <input type="hidden" name="id_pengguna" class="form-control" value="<?=$profil->id_user?>">
            <input type="text" name="nama_pengguna" class="form-control" placeholder="Masukkan Nama Pengguna" value="<?=$profil->nama_user?>">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" placeholder="Masukkan Email" value="<?=$profil->email?>">
          </div>
          <div class="form-group">
            <label>No HP</label>
            <input type="text" name="no_hp" class="form-control" placeholder="Masukkan No HP" value="<?=$profil->no_hp?>">
          </div>
          <button type="submit" class="btn btn-primary tambah"><i class="glyphicon glyphicon-floppy-save"></i> Simpan</button>
          <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div><div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title">Edit Password</h3>
      </div>
      <div class="modal-body">
        <form id="formupdate" action="<?= base_url()?>Profil/update_password" method="post">
          <div class="form-group">
            <label>Password</label>
            <input type="hidden" name="id_pengguna" class="form-control" value="<?=$profil->id_user?>">
            <input type="text" name="password" class="form-control" placeholder="Masukkan Password Baru" required="">
          </div>
          <button type="submit" class="btn btn-primary update"><i class="glyphicon glyphicon-floppy-save"></i> Simpan</button>
          <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="container">
	<br>
	<input type="hidden" name="pass" value="<?=$profil->nik?>">
	<div class="panel panel-primary" style="border-radius: 10px">
		<div class="panel panel-heading" style="border-radius: 10px">
			<h3 align="center"><b>Profil</b></h3>
		</div>
		<div class="panel panel-body" style="border-radius: 10px">
		<center>
			<table style="font-size: 20px">
				<tr>
					<th>Username</th>
					<th style="width: 50px">&emsp;:</th>
					<th><?=$profil->nama_user?></th>
				</tr>
				<tr>
					<th>Email</th>
					<th>&emsp;:</th>
					<th><?=$profil->email?></th>
				</tr>
				<tr>
					<th>No HP</th>
					<th>&emsp;:</th>
					<th><?=$profil->no_hp?></th>
				</tr>
			</table>
			<br>
			<br>
				<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal1" style="border-radius: 50px"><i class="fa fa-user"></i> &nbsp;<b>Edit Nama & Kontak</b></button>&emsp;
				<button class="btn btn-danger btn-sm password" style="border-radius: 50px"><i class="fa fa-lock"></i> &nbsp;<b>Edit Password</b></button>
		</div>
			</center>
	</div>
</div>

<script type="text/javascript">
  $(document).ready(function()
  {
    $('.password').on('click',function()
    {
      var id = $('[name="pass"]').val();
      swal("Masukkan NIK Untuk Memastikan Akun Asli Yang Merubah Password","", {
        icon : 'warning',
      content: {
        element: "input",
        attributes: {
          placeholder: "Masukkan NIK",
          type: "text",
        },
      },
      })
        .then((value) => {
        if (`${value}` == id)
        {
            update_password(id);
        }
        else
        {
            swal("NIK Salah !!!","","error");
        }
        });
    })
    function update_password(id)
    {
      $('.edit_alumni_modal').click();
    }
  })
</script>