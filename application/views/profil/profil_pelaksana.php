<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
      	<div class="pull-left"></div>
      	<div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judulmodal"></h3>
      </div>
      <div class="modal-body">
    	<form action="<?=base_url()?>Profil_pelaksana/update_profil_pelaksana" method="post" id="formupdate">
    		<input type="hidden" name="tahun" value="<?=$tahun?>">
    		<input type="hidden" name="idpelaksana" value="">
    	<input type="hidden" name="target" value="">
		  <div class="form-group">
		  	<label>Masukkan Deskripsi</label>
	  		<textarea class="form-control" name="deskripsi" placeholder="Masukkan Deskripsi" required=""></textarea>
	  	</div>
		  <button type="submit" class="btn btn-primary update btn-sm"><i class="glyphicon glyphicon-ok"></i></button>
		  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></button>
		</form>
      </div>
    </div>
  </div>
</div>
<br>
<div style="width: 300px" class="pull-right">
<form action="<?=base_url()?>Profil_pelaksana/" method="post">
<div class="col-md-8">
	<select name="tahun" class="form-control" required="">
	<?php
	for($i=2018;$i<2031;$i++)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	?>
	</select>
</div>
<div class="col-md-1"><button type="submit" class="btn btn-success"><b>Jelajahi</b></button></div>
</form>
</div>
<br><br>
<div class="panel panel-primary">
	<div class="panel panel-heading">
		<h3 align="center">PROFIL JABATAN PELAKSANA <?=$tahun?></h3>
	</div>
		<div class="panel-body table-responsive">
		<table class="table table-hover table-bordered">
			<thead style="font-weight: bold;text-align: center;">
				<tr class="btn-success">
					<td>No</td>
					<td>Instansi Pembina</td>
					<td>Nama Jabatan</td>
					<td>Urusan</td>
					<td>Tugas Jabatan</td>
					<td>Uraian Tugas Jabatan</td>
					<td>Kualifikasi Pendidikan</td>
					<td>Kompetensi Teknis Jabatan</td>
					<td>Syarat Jabatan</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(empty($profil))
				{
					echo '<tr><th><br><center>Data Kosong</center></th></tr>';
				}
				else
				{
				$i = 1;
				foreach ($profil as $key => $pro) 
				{?>
					<tr align="center">
						<td><?=$i++?></td>
						<td><?=$pro->nama_instansi?></td>
						<td><?=$pro->nama_jabatan?></td>
						<td>
							<?php 
							if($pro->urusan == null)
							{?>
								<i style="cursor:pointer" class="fa fa-pencil text-red opsi" id="<?=$pro->id_profil_pelaksana?>" link="urusan"></i>
							<?php
							}
							else
							{?>
								<form action="<?=base_url()?>Profil_pelaksana/update_profil_pelaksana" method="post">
    		<input type="hidden" name="tahun" value="<?=$tahun?>">
						    	<input type="hidden" name="idpelaksana" value="<?=$pro->id_profil_pelaksana?>">
						    	<input type="hidden" name="target" value="urusan">
									<textarea class="form-control" name="deskripsi"><?=$pro->urusan?></textarea>
									<button type="submit" class="btn btn-info btn-xs btn-block">Update</button>
								</form>
							<?php
							}
							?>
						</td>
						<td>
							<?php 
							if($pro->tug_jab == null)
							{?>
								<i style="cursor:pointer" class="fa fa-pencil text-red  opsi" id="<?=$pro->id_profil_pelaksana?>" link="tug_jab"></i>
							<?php
							}
							else
							{?>
								<form action="<?=base_url()?>Profil_pelaksana/update_profil_pelaksana" method="post">
    		<input type="hidden" name="tahun" value="<?=$tahun?>">
						    	<input type="hidden" name="idpelaksana" value="<?=$pro->id_profil_pelaksana?>">
						    	<input type="hidden" name="target" value="tug_jab">
									<textarea class="form-control" name="deskripsi"><?=$pro->tug_jab?></textarea>
									<button type="submit" class="btn btn-info btn-xs btn-block">Update</button>
								</form>
							<?php
							}
							?>
						</td>
						<td>
							<?php 
							if($pro->urai_tug == null)
							{?>
								<i style="cursor:pointer" class="fa fa-pencil text-red  opsi" id="<?=$pro->id_profil_pelaksana?>" link="urai_tug"></i>
							<?php
							}
							else
							{?>
								<form action="<?=base_url()?>Profil_pelaksana/update_profil_pelaksana" method="post">
    		<input type="hidden" name="tahun" value="<?=$tahun?>">
						    	<input type="hidden" name="idpelaksana" value="<?=$pro->id_profil_pelaksana?>">
						    	<input type="hidden" name="target" value="urai_tug">
									<textarea class="form-control" name="deskripsi"><?=$pro->urai_tug?></textarea>
									<button type="submit" class="btn btn-info btn-xs btn-block">Update</button>
								</form>
							<?php
							}
							?>
						</td>
						<td>
							<?php 
							if($pro->kual_pend == null)
							{?>
								<i style="cursor:pointer" class="fa fa-pencil text-red  opsi" id="<?=$pro->id_profil_pelaksana?>" link="kual_pend"></i>
							<?php
							}
							else
							{?>
								<form action="<?=base_url()?>Profil_pelaksana/update_profil_pelaksana" method="post">
    		<input type="hidden" name="tahun" value="<?=$tahun?>">
						    	<input type="hidden" name="idpelaksana" value="<?=$pro->id_profil_pelaksana?>">
						    	<input type="hidden" name="target" value="kual_pend">
									<textarea class="form-control" name="deskripsi"><?=$pro->kual_pend?></textarea>
									<button type="submit" class="btn btn-info btn-xs btn-block">Update</button>
								</form>
							<?php
							}
							?>
						</td>
						<td>
							<?php 
							if($pro->komp_tek == null)
							{?>
								<i style="cursor:pointer" class="fa fa-pencil text-red  opsi" id="<?=$pro->id_profil_pelaksana?>" link="komp_tek"></i>
							<?php
							}
							else
							{?>
								<form action="<?=base_url()?>Profil_pelaksana/update_profil_pelaksana" method="post">
    		<input type="hidden" name="tahun" value="<?=$tahun?>">
						    	<input type="hidden" name="idpelaksana" value="<?=$pro->id_profil_pelaksana?>">
						    	<input type="hidden" name="target" value="komp_tek">
									<textarea class="form-control" name="deskripsi"><?=$pro->komp_tek?></textarea>
									<button type="submit" class="btn btn-info btn-xs btn-block">Update</button>
								</form>
							<?php
							}
							?>
						</td>
						<td>
							<?php 
							if($pro->syarat_jab == null)
							{?>
								<i style="cursor:pointer" class="fa fa-pencil text-red  opsi" id="<?=$pro->id_profil_pelaksana?>" link="syarat_jab"></i>
							<?php
							}
							else
							{?>
								<form action="<?=base_url()?>Profil_pelaksana/update_profil_pelaksana" method="post">
    		<input type="hidden" name="tahun" value="<?=$tahun?>">
						    	<input type="hidden" name="idpelaksana" value="<?=$pro->id_profil_pelaksana?>">
						    	<input type="hidden" name="target" value="syarat_jab">
									<textarea class="form-control" name="deskripsi"><?=$pro->syarat_jab?></textarea>
									<button type="submit" class="btn btn-info btn-xs btn-block">Update</button>
								</form>
							<?php
							}
							?>
						</td>
					</tr>
				<?php } }?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.opsi').on('click',function()
		{
			$('#exampleModal1').modal();
			var id = $(this).attr('link');
			var id1 = $(this).attr('id');
			if(id == "urusan")
			{
				$('.judulmodal').html('Update Urusan');
			}
			else if(id == "tug_jab")
			{
				$('.judulmodal').html('Update Tugas Jabatan');
			}
			else if(id == "urai_tug")
			{
				$('.judulmodal').html('Update Uraian Tugas Jabatan');
			}
			else if(id == "kual_pend")
			{
				$('.judulmodal').html('Update Kualifikasi Pendidikan');
			}
			else if(id == "komp_tek")
			{
				$('.judulmodal').html('Update Kompetensi Teknis Jabatan');
			}
			else if(id == "syarat_jab")
			{
				$('.judulmodal').html('Update Syarat Jabatan');
			}
			$.ajax({
				type : 'POST',
				url : '<?=base_url()?>Profil_pelaksana/edit_profil_pelaksana/'+id,
				data : {id1:id1},
				success : function(data)
				{
					console.log(data);
					$('[name="target"]').val(id);
					$('[name="idpelaksana"]').val(id1);
					$('[name="deskripsi"]').html(data);
				}
			})
		})
	})
</script>