<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
      	<div class="pull-left"></div>
      	<div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judulmodal">Update Data</h3>
      </div>
      <div class="modal-body">
    	<form action="<?=base_url()?>Profil_jf/update_profil_jf" method="post" id="formupdate" enctype= "multipart/form-data">
    	<input type="hidden" name="id_profil_jf" value="">
    	<input type="hidden" name="tahun" value="<?=$tahun?>">
    	<input type="hidden" name="instansi" value="">
    	<input type="hidden" name="jabatan" value="">
		  <div class="form-group">
		  	<label>Kedudukan</label>
		  	<div class="form-group">
			  	<input type="checkbox" name="kedudukan[]" value="Pusat"> Pusat <br>
			  	<input type="checkbox" name="kedudukan[]" value="Daerah"> Daerah <br>
			  	<input type="checkbox" name="kedudukan[]" value="Pemerintah"> Pemerintah <br>
		  	</div>
			</div>
		  <div class="form-group">
		  	<label>Kompetensi Teknis</label>
				<textarea name="kompetensi_teknis" class="form-control"></textarea>
			</div>
		  <div class="form-group">
		  	<label>Kategori</label><br>
				<input type="checkbox" class="check_keahlian" name="kategori[]" value="keahlian"> Keahlian<br>
				<input type="checkbox" class="check_keterampilan" name="kategori[]" value="keterampilan"> Keterampilan
			</div>
		  <fieldset class="form-group">
		  	<div class="keahlian col-md-6">
		  		<label>Keahlian</label><br>
			  	<input type="checkbox" name="jenjang_jabatan_keahlian[]" value="Ahli Utama"> Ahli Utama<br>
			  	<input type="checkbox" name="jenjang_jabatan_keahlian[]" value="Ahli Madya"> Ahli Madya<br>
			  	<input type="checkbox" name="jenjang_jabatan_keahlian[]" value="Ahli Muda"> Ahli Muda<br>
			  	<input type="checkbox" name="jenjang_jabatan_keahlian[]" value="Ahli Pertama"> Ahli Pertama<hr>
		  	</div>
		  	<div class="keterampilan col-md-6">
		  		<label>Keterampilan</label><br>
			  	<input type="checkbox" name="jenjang_jabatan_keterampilan[]" value="Penyelia"> Penyelia<br>
			  	<input type="checkbox" name="jenjang_jabatan_keterampilan[]" value="Pelaksana Lanjutan"> Pelaksana Lanjutan<br>
			  	<input type="checkbox" name="jenjang_jabatan_keterampilan[]" value="Pelaksana"> Pelaksana<br>
			  	<input type="checkbox" name="jenjang_jabatan_keterampilan[]" value="Pelaksana Pemula"> Pelaksana Pemula<hr>
		  	</div>
	  	</fieldset>
	<div class="form-group">
		<label>Nomor Permenpanrb</label>
		<input type="text" class="form-control" name="permenpanrb">
	</div>
	<div class="form-group">
		<label>Junlak Junkis</label>
		<input type="text" class="form-control" name="junlak_junkis">
	</div>
	<div class="form-group">
		<label>Tugas Pokok</label>
		<input type="text" class="form-control" name="tugas_pokok">
	</div>
	<div class="form-group">
		<label>Perpres Tunjangan</label>
		<input type="text" class="form-control" name="perpres_tunjangan">
	</div>
	<div class="form-group">
		<label>Pengaturan Bup</label>
		<input type="text" class="form-control" name="pengaturan_bup">
	</div>
	<div class="form-group">
		<label>Pengangkatan Pertama</label>
		<input type="file" class="form-control" name="pengangkatan_pertama">
	</div>
	<div class="form-group">
		<label>Pengangkatan Penyesuaian</label>
		<input type="file" class="form-control" name="pengangkatan_penyesuaian">
	</div>
	<div class="form-group">
		<label>Perpindahan Jabatan Lain</label>
		<input type="file" class="form-control" name="perpndahan_jabatan_lain">
	</div>
<!-- 	<div class="form-group">
		<label>Promosi</label>
		<input type="file" class="form-control" name="promosi">
	</div>
	<div class="form-group">
		<label>Penialaian Kerja</label>
		<input type="file" class="form-control" name="penialaian_kerja">
	</div>
	<div class="form-group">
		<label>Pemberhentian</label>
		<input type="file" class="form-control" name="pemberhentian">
	</div>
	<div class="form-group">
		<label>Pengangkatan Kembali</label>
		<input type="file" class="form-control" name="pengangkatan_kembali">
	</div>
	<div class="form-group">
		<label>Jenjang, Angka, Kredit, Tunjangan Fungsional</label>
		<input type="file" class="form-control" name="jenjang_tunjangan_fungsional">
	</div>
 -->		  <button type="submit" class="btn btn-primary update btn-sm"> <i class="glyphicon glyphicon-ok"></i> <b>Simpan </b></button>
		  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i> <b>Batal </b></button>
		</form>
      </div>
    </div>
  </div>
</div>

<div style="width: 300px" class="pull-right">
	<form action="<?=base_url()?>Profil_jf/" method="post">
<div class="col-md-8">
	<select name="tahun" class="form-control" required="">
	<?php
	for($i=2018;$i<2031;$i++)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	?>
	</select>
</div>
	<div class="col-md-1"><button type="submit" class="btn btn-success"><b>Jelajahi</b></button></div>
	</form>
</div>
<br><br>

<div class="panel panel-primary">
	<div class="panel panel-heading">
		<h3 align="center">PROFIL JF Tahun <?=$tahun?></h3>
	</div>
		<div class="panel-body table-responsive">
		<table class="table table-hover table-bordered" id="example1">
			<thead style="font-weight: bold;text-align: center;">
				<tr class="btn-success">
					<td rowspan="2">No</td>
					<td rowspan="2">Instansi Pembina</td>
					<td rowspan="2">Nama Jabatan</td>
					<td rowspan="2">Kategori</td>
					<td rowspan="2">Kedudukan</td>
					<td rowspan="2">Kompetensi Teknis</td>
					<td rowspan="2">Jenjang Jabatan</td>
					<td colspan="2">Jumlah Pejabat</td>

					<td rowspan="2">No. Permenpanrb</td>
					<td rowspan="2">Junlak Junkis</td>
					<td rowspan="2">Tugas Pokok</td>
					<td rowspan="2">Perpres Tunjangan</td>
					<td rowspan="2">Peraturan Bup</td>
					<td colspan="4">Pengangkatan</td>
					<td rowspan="2">Penilaian Kerja</td>
					<td rowspan="2">Pemberhentian</td>
					<td rowspan="2">Pengangkatan Kembali</td>
					<td rowspan="2">Jenjang, Angka, Kredit, Tunjangan Fungsional</td>

				</tr>
				<tr class="btn-primary">
					<td>Aktif</td>
					<td>Proyeksi</td>
					<td>Pengangkatan Pertama</td>
					<td>Pengangkatan Penyesuaian</td>
					<td>Perpindahan Jabatan Lain</td>
					<td>Promosi</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(empty($profil))
				{
					echo '<tr align="center">
									<td><br><b>Data Kosong</b></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>';
				}
				else
				{
				$i = 1;
				// print_r($profil);
				foreach ($profil as $key => $pro) 
				{
					$hak = $this->session->userdata('hak_akses');
				?>
					<tr>
						<td align="center"><?=$i++?></td>
						<td align="center"><?=$pro->nama_instansi?>
				<?php
					$hak = $this->session->userdata('hak_akses');
			 		if ($hak != "admin")
			 		{
						if($hak == $pro->id_instansi)
						{?>
								<button class="btn btn-info btn-xs btn-block jenjang" link="<?=$pro->id_profil_jf?>" style="cursor: pointer;">Edit</button>
						<?php
						}
						else
						{
							echo ''	;
						}
			 		}
			 		else
			 		{
		 		?>
		 			<button class="btn btn-info btn-xs btn-block jenjang" link="<?=$pro->id_profil_jf?>" style="cursor: pointer;">Edit</button>
		 		<?php 
		 		}
				?>
					</td>
						<td><?=$pro->nama_jabatan?></td>
						<td>
							<?php
							$kategori_ = json_decode($pro->kategori,true);
							if(empty($kategori_[0]))
								echo "Belum Tersedia";
							else
							{
								$no_kat = 1;
								foreach ($kategori_ as $key => $ktg) 
								{
									echo ' * ' . $ktg . '<br>';
								}
							}
							?></td>
						<td>
							<?php
							if(empty($pro->kedudukan))
								echo "Belum Tersedia";
							else
								$kedudukan = json_decode($pro->kedudukan);
								foreach ($kedudukan as $key => $kdk) 
								{
									echo ' * ' . $kdk . '<br>';
								}
							?></td>
						<td>
							<?php
							if(empty($pro->kompetensi_teknis))
								echo "Belum Tersedia";
							else
								echo $pro->kompetensi_teknis;
							?></td>
						<td>
							<?php
							if(empty($pro->jenjang_jabatan))
								echo "Belum Tersedia";
							else
							{
								$jenjang_ = json_decode($pro->jenjang_jabatan,true);
								$no_jj = 1;
								foreach ($jenjang_ as $key => $jj) 
								{

									if(!empty($jj['keahlian']))
									{
										if($jj['keahlian']=="null")
										{						
										}
										else
										{
											echo '<b>Keahlian</b><br>';

											foreach (json_decode($jj['keahlian']) as $key => $keah) 
											{
												if($keah==null)
													echo '&emsp; - ';
												else
													echo '&emsp; * ' . $keah . '<br>';
											}				
										}
									}

									if(!empty($jj['keterampilan']))
									{
										if($jj['keterampilan']=="null")
										{						
										}
										else
										{
											echo '<b>Keterampilan</b><br>';

											foreach (json_decode($jj['keterampilan']) as $key => $ket) 
											{
												if($ket==null)
													echo '&emsp; - ';
												else
													echo '&emsp; * ' . $ket . '<br>';
											}
										}
									}
								}
							}
							?></td>
						<td align="center">
							<?php
								$aktif = $this->db->query("select * from pejafung where id_instansi=$pro->id_instansi and id_jabatan=$pro->id_jabatan")->row();
								$ahli_utama_akt = json_decode($aktif->ahli_utama,true); 
								$ahli_utama_akt1 = intval($ahli_utama_akt[0]['pusatatas50']);
								$ahli_utama_akt2 = intval($ahli_utama_akt[0]['pusatbawah50']);
								$ahli_utama_akt3 = intval($ahli_utama_akt[0]['provatas50']);
								$ahli_utama_akt4 = intval($ahli_utama_akt[0]['provbawah50']);
								$ahli_utama_akt5 = intval($ahli_utama_akt[0]['kabatas50']);
								$ahli_utama_akt6 = intval($ahli_utama_akt[0]['kabbawah50']);
								$total_ahli_utama_akt =  $ahli_utama_akt1 + $ahli_utama_akt2 + $ahli_utama_akt3 + $ahli_utama_akt4 + $ahli_utama_akt5 + $ahli_utama_akt6;


								$ahli_madya_akt = json_decode($aktif->ahli_madya,true); 
								$ahli_madya_akt1 = intval($ahli_madya_akt[0]['pusatatas50']);
								$ahli_madya_akt2 = intval($ahli_madya_akt[0]['pusatbawah50']);
								$ahli_madya_akt3 = intval($ahli_madya_akt[0]['provatas50']);
								$ahli_madya_akt4 = intval($ahli_madya_akt[0]['provbawah50']);
								$ahli_madya_akt5 = intval($ahli_madya_akt[0]['kabatas50']);
								$ahli_madya_akt6 = intval($ahli_madya_akt[0]['kabbawah50']);
								$total_ahli_madya_akt =  $ahli_madya_akt1 + $ahli_madya_akt2 + $ahli_madya_akt3 + $ahli_madya_akt4 + $ahli_madya_akt5 + $ahli_madya_akt6;


								$ahli_muda_akt = json_decode($aktif->ahli_muda,true); 
								$ahli_muda_akt1 = intval($ahli_muda_akt[0]['pusatatas50']);
								$ahli_muda_akt2 = intval($ahli_muda_akt[0]['pusatbawah50']);
								$ahli_muda_akt3 = intval($ahli_muda_akt[0]['provatas50']);
								$ahli_muda_akt4 = intval($ahli_muda_akt[0]['provbawah50']);
								$ahli_muda_akt5 = intval($ahli_muda_akt[0]['kabatas50']);
								$ahli_muda_akt6 = intval($ahli_muda_akt[0]['kabbawah50']);
								$total_ahli_muda_akt = $ahli_muda_akt1 + $ahli_muda_akt2 + $ahli_muda_akt3 + $ahli_muda_akt4 + $ahli_muda_akt5 + $ahli_muda_akt6;


								$ahli_pertama_akt = json_decode($aktif->ahli_pertama,true); 
								$ahli_pertama_akt1 = intval($ahli_pertama_akt[0]['pusatatas50']);
								$ahli_pertama_akt2 = intval($ahli_pertama_akt[0]['pusatbawah50']);
								$ahli_pertama_akt3 = intval($ahli_pertama_akt[0]['provatas50']);
								$ahli_pertama_akt4 = intval($ahli_pertama_akt[0]['provbawah50']);
								$ahli_pertama_akt5 = intval($ahli_pertama_akt[0]['kabatas50']);
								$ahli_pertama_akt6 = intval($ahli_pertama_akt[0]['kabbawah50']);
								$total_ahli_pertama_akt = $ahli_pertama_akt1 + $ahli_pertama_akt2 + $ahli_pertama_akt3 + $ahli_pertama_akt4 + $ahli_pertama_akt5 + $ahli_pertama_akt6;


								$penyelia_akt = json_decode($aktif->penyelia,true); 
								$penyelia_akt1 = intval($penyelia_akt[0]['pusatatas50']);
								$penyelia_akt2 = intval($penyelia_akt[0]['pusatbawah50']);
								$penyelia_akt3 = intval($penyelia_akt[0]['provatas50']);
								$penyelia_akt4 = intval($penyelia_akt[0]['provbawah50']);
								$penyelia_akt5 = intval($penyelia_akt[0]['kabatas50']);
								$penyelia_akt6 = intval($penyelia_akt[0]['kabbawah50']);
								$total_penyelia_akt = $penyelia_akt1 + $penyelia_akt2 + $penyelia_akt3 + $penyelia_akt4 + $penyelia_akt5 + $penyelia_akt6;


								$pelaksana_lanjutan_akt = json_decode($aktif->pelaksana_lanjutan,true); 
								$pelaksana_lanjutan_akt1 = intval($pelaksana_lanjutan_akt[0]['pusatatas50']);
								$pelaksana_lanjutan_akt2 = intval($pelaksana_lanjutan_akt[0]['pusatbawah50']);
								$pelaksana_lanjutan_akt3 = intval($pelaksana_lanjutan_akt[0]['provatas50']);
								$pelaksana_lanjutan_akt4 = intval($pelaksana_lanjutan_akt[0]['provbawah50']);
								$pelaksana_lanjutan_akt5 = intval($pelaksana_lanjutan_akt[0]['kabatas50']);
								$pelaksana_lanjutan_akt6 = intval($pelaksana_lanjutan_akt[0]['kabbawah50']);
								$total_pelaksana_lanjutan_akt = $pelaksana_lanjutan_akt1 + $pelaksana_lanjutan_akt2 + $pelaksana_lanjutan_akt3 + $pelaksana_lanjutan_akt4 + $pelaksana_lanjutan_akt5 + $pelaksana_lanjutan_akt6;


								$terampil_akt = json_decode($aktif->terampil,true); 
								$terampil_akt1 = intval($terampil_akt[0]['pusatatas50']);
								$terampil_akt2 = intval($terampil_akt[0]['pusatbawah50']);
								$terampil_akt3 = intval($terampil_akt[0]['provatas50']);
								$terampil_akt4 = intval($terampil_akt[0]['provbawah50']);
								$terampil_akt5 = intval($terampil_akt[0]['kabatas50']);
								$terampil_akt6 = intval($terampil_akt[0]['kabbawah50']);
								$total_terampil_akt = $terampil_akt1 + $terampil_akt2 + $terampil_akt3 + $terampil_akt4 + $terampil_akt5 + $terampil_akt6;


								$pelaksana_pemula_akt = json_decode($aktif->pelaksana_pemula,true); 
								$pelaksana_pemula_akt1 = intval($pelaksana_pemula_akt[0]['pusatatas50']);
								$pelaksana_pemula_akt2 = intval($pelaksana_pemula_akt[0]['pusatbawah50']);
								$pelaksana_pemula_akt3 = intval($pelaksana_pemula_akt[0]['provatas50']);
								$pelaksana_pemula_akt4 = intval($pelaksana_pemula_akt[0]['provbawah50']);
								$pelaksana_pemula_akt5 = intval($pelaksana_pemula_akt[0]['kabatas50']);
								$pelaksana_pemula_akt6 = intval($pelaksana_pemula_akt[0]['kabbawah50']);
								$total_pelaksana_pemula_akt = $pelaksana_pemula_akt1 + $pelaksana_pemula_akt2 + $pelaksana_pemula_akt3 + $pelaksana_pemula_akt4 + $pelaksana_pemula_akt5 + $pelaksana_pemula_akt6;

								$total_aktif = $total_ahli_utama_akt + $total_ahli_madya_akt + $total_ahli_muda_akt + $total_ahli_pertama_akt + $total_penyelia_akt + $total_pelaksana_lanjutan_akt + $total_terampil_akt + $total_pelaksana_pemula_akt;
								echo $total_aktif;
							?>
						</td>
						<td align="center">
							<?php
								$proyeksi = $this->db->query("select * from proyeksi where id_instansi=$pro->id_instansi and id_jabatan=$pro->id_jabatan")->row();
								$total_proyeksi = $proyeksi->ahli_utama+$proyeksi->ahli_madya+$proyeksi->ahli_muda+$proyeksi->ahli_pertama+$proyeksi->penyelia+$proyeksi->pelaksana_lanjutan+$proyeksi->terampil+$proyeksi->pelaksana_pemula;
								echo $total_proyeksi;
							?>
						</td>

						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				<?php } }?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function()
{	
		$('.jenjang').on('click',function()
		{
			var id = $(this).attr('link');
			$.ajax({
				url : '<?=base_url()?>Profil_jf/edit_profil_jf/'+id,
				dataType : 'JSON',
				success : function(data)
				{
					$('[name="instansi"]').val(data.id_instansi);
					$('[name="jabatan"]').val(data.id_jabatan);
					$('[name="kompetensi_teknis"]').val(data.kompetensi_teknis);

					var kedudukan = data.kedudukan;
					obj = JSON.parse(kedudukan);
					console.log(kedudukan);
					for(j=0;j<obj.length;j++)
					{
						$('[value="'+obj[j]+'"]').attr('checked','checked');
					}

					var jenjang = data.jenjang_jabatan;
					obj = JSON.parse(jenjang);
					if(obj[0].keterampilan)
					{
						if(obj[0].keterampilan=="null")
						{
						}
						else
						{
							obj_ket = JSON.parse(obj[0].keterampilan);
							for(j=0;j<obj_ket.length;j++)
							{
								$('[value="'+obj_ket[j]+'"]').attr('checked','checked');
							}
						}
					}
					else if(obj[1].keterampilan)
					{
						if(obj[1].keterampilan=="null")
						{
						}
						else
						{
							obj_ket_ = JSON.parse(obj[1].keterampilan);
							for(j=0;j<obj_ket_.length;j++)
							{
								$('[value="'+obj_ket_[j]+'"]').attr('checked','checked');
							}
						}
					}

					if(obj[0].Keahlian)
					{
						if(obj[0].keahlian=="null")
						{
						}
						else
						{
							obj_keah = JSON.parse(obj[0].keahlian);
							for(j=0;j<obj_keah.length;j++)
							{
								$('[value="'+obj_keah[j]+'"]').attr('checked','checked');
							}
						}
					}
					else if(obj[1].keahlian)
					{
						if(obj[1].keahlian=="null")
						{
						}
						else
						{
							obj_keah_ = JSON.parse(obj[1].keahlian);
							for(j=0;j<obj_keah_.length;j++)
							{
								$('[value="'+obj_keah_[j]+'"]').attr('checked','checked');
							}
						}
					}		

					var kategori = data.kategori;
					kt = JSON.parse(kategori);
					for(j=0;j<kt.length;j++)
					{
						$('[value="'+kt[j]+'"]').attr('checked','checked');
					}

				}
			})
			
			$('[name="id_profil_jf"]').val(id);
			$('#exampleModal1').modal();
		})


})
</script>