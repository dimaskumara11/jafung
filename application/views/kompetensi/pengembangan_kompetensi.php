<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
      	<div class="pull-left"></div>
      	<div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judulmodal"></h3>
      </div>
      <div class="modal-body">
      	<div class="edit_pp"></div>
      </div>
    </div>
  </div>
</div>
<div style="width: 500px" class="pull-right">
<form action="<?=base_url()?>Kompetensi/" method="post">
<div class="col-md-4">
	<select name="tahun_mulai" class="form-control" required="">
	<?php
	for($i=2018;$i<2031;$i++)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	?>
	</select>
</div>
<div class="col-md-4">
	<select name="tahun_sampai" class="form-control" required="">
	<?php
	for($i=2018;$i<2031;$i++)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	?>
	</select>
</div>
<div class="col-md-1"><button type="submit" class="btn btn-success"><b>Jelajahi</b></button></div>
</form>
</div>
<br>
<br>
<!-- <div class="content"><button class="btn btn-success" data-toggle="modal" data-target="#exampleModal1"><b><i class="fa fa-plus"></i> Tambah Tahun</b></button><br><br> -->
<input type="hidden" name="tahun_mulai_" value="<?=$no1?>">
<input type="hidden" name="tahun_sampai_" value="<?=$no4?>">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 align="center">PENGEMBANGAN KOMPETENSI PEJABAT FUNGSIONAL TAHUN <?=$tahun?></h3>
	</div>
	<div class="panel-body table-responsive">
		<table class="table table-hover table-bordered">
			<thead style="font-weight: bold;text-align: center;">
				<tr class="btn-success">
					<td rowspan="3">No</td>
					<td rowspan="3">Nama Jabatan</td>
					<td rowspan="3">Kompetensi Teknis Per Jenjang Jabatan</td>
					<td rowspan="3">Instansi Pembina</td>
					<td colspan="<?=$no2?>">Diklat Fungsional</td>
					<td colspan="<?=$no2?>">Diklat Teknis</td>
					<td rowspan="2" colspan="<?=$no3?>">Diklat Lainnya</td>
				</tr>
				<tr class="btn-warning">
					<td colspan="<?=$no3?>">Jumlah Diklat Fungsional</td>
					<td colspan="<?=$no3?>">Jumlah Peserta</td>
					<td colspan="<?=$no3?>">Jumlah Diklat Teknis</td>
					<td colspan="<?=$no3?>">Jumlah Peserta</td>
				</tr>
				<tr class="btn-info">
					<?php for($a=$no1;$a<=$no4;$a++)
					{
					echo	'<td>'.$a.'</td>';
					}?>
					<?php for($a=$no1;$a<=$no4;$a++)
					{
					echo	'<td>'.$a.'</td>';
					}?>
					<?php for($a=$no1;$a<=$no4;$a++)
					{
					echo	'<td>'.$a.'</td>';
					}?>
					<?php for($a=$no1;$a<=$no4;$a++)
					{
					echo	'<td>'.$a.'</td>';
					}?>
					<?php for($a=$no1;$a<=$no4;$a++)
					{
					echo	'<td>'.$a.'</td>';
					}?>
				</tr>
			</thead>
			<tbody>
			<?php
			if(empty($pengembangan))
			{
				echo '<tr><th><center><br>Data Kosong</center></th></tr>';
			} 
			else
			{
				$num=1; foreach ($pengembangan as $key => $pg) 
				{
				?>
					<tr align="center">
						<td><?=$num++?></td>
						<td><?=$pg->nama_jabatan?></td>
						<td>
								<form action="<?=base_url()?>Kompetensi/update_pejafung_pk1" method="post">
						    	<input type="hidden" name="id_pengembangan" value="<?=$pg->id_pengembangan?>">
						    	<input type="hidden" name="instansi" value="<?=$pg->id_instansi?>">
						    	<input type="hidden" name="jabatan" value="<?=$pg->id_jabatan?>">
						    	<input type="hidden" name="tahun" value="<?=$pg->tahun?>">
									<input type="hidden" name="tahun_mulai_" value="<?=$no1?>">
									<input type="hidden" name="tahun_sampai_" value="<?=$no4?>">
									<textarea class="form-control" name="deskripsi"><?=$pg->komp_tek?></textarea>
									<button type="submit" class="btn btn-info btn-xs btn-block">Update</button>
								</form>
						</td>
						<td><?=$pg->nama_instansi?></td>
						<?php for($a=$no1;$a<=$no4;$a++)
						{
							$kompetensi = $this->db->get_where('pengembangan_kompetensi',array('id_jabatan'=>$pg->id_jabatan,'tahun'=>$a))->row_array();
							// print_r($kompetensi);
							$dik_fung = json_decode($kompetensi['diklat_fung']);
							if ($dik_fung == null)
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'" class="jum_dik_fung" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal"> 0 </td>';
							}
							else
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'" class="jum_dik_fung" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal">'.$dik_fung[0]->jum_dik_fung.'</td>';
							}

						}?>
						<?php for($a=$no1;$a<=$no4;$a++)
						{
							$kompetensi = $this->db->get_where('pengembangan_kompetensi',array('id_jabatan'=>$pg->id_jabatan,'tahun'=>$a))->row_array();
							$dik_fung = json_decode($kompetensi['diklat_fung']);
							if ($dik_fung == null)
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'" class="jum_pes1" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal"> 0 </td>';
							}
							else
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'" class="jum_pes1" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal">'.$dik_fung[0]->jum_pes.'</td>';
							}
						}?>
						<?php for($a=$no1;$a<=$no4;$a++)
						{
							$kompetensi = $this->db->get_where('pengembangan_kompetensi',array('id_jabatan'=>$pg->id_jabatan,'tahun'=>$a))->row_array();
							$dik_tek = json_decode($kompetensi['diklat_tek']);
							if ($dik_tek == null)
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'" class="jum_dik_tek" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal"> 0 </td>';
							}
							else
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'" class="jum_dik_tek" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal">'.$dik_tek[0]->jum_dik_tek.'</td>';
							}
						}?>
						<?php for($a=$no1;$a<=$no4;$a++)
						{
							$kompetensi = $this->db->get_where('pengembangan_kompetensi',array('id_jabatan'=>$pg->id_jabatan,'tahun'=>$a))->row_array();
							$dik_tek = json_decode($kompetensi['diklat_tek']);
							if ($dik_tek == null)
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'" class="jum_pes2" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal"> 0 </td>';;
							}
							else
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'" class="jum_pes2" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal">'.$dik_tek[0]->jum_pes.'</td>';
							}
						}?>
						<?php for($a=$no1;$a<=$no4;$a++)
						{
							$kompetensi = $this->db->get_where('pengembangan_kompetensi',array('id_jabatan'=>$pg->id_jabatan,'tahun'=>$a))->row_array();
							if ($kompetensi['dik_lain'] == null)
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'"class="diklat_lain" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal"> 0 </td>';;
							}
							else
							{
								echo '<td link="'.$a.'" jabatan="'.$pg->id_jabatan.'" instansi="'.$pg->id_instansi.'" id="'.$kompetensi["id_pengembangan"].'"class="diklat_lain" style="cursor: pointer" data-toggle="modal" data-target="#exampleModal">'.$kompetensi['dik_lain'].'</td>';
							}
						}?>
					</tr>
			<?php } }?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
		$('.jum_dik_fung').on('click',function()
		{
			var mul = $('[name="tahun_mulai_"]').val();
			var smp = $('[name="tahun_sampai_"]').val();
			var id = $(this).attr('id');
			if (id == 0)
			{
				var id = 0;
			}
			var th = $(this).attr('link');
			var jb = $(this).attr('jabatan');
			var ins = $(this).attr('instansi');
			// console.log(jb);
			$.ajax({
				type : 'post',
				url : '<?= base_url()?>Kompetensi/edit_pejafung_pk/'+id+'/diklat_fung/jum_dik_fung/jum_pes/'+th,
				data : {jb:jb,ins:ins,mul:mul,smp:smp},
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Update Jumlah Pejafung');
				}
			})
		})
		$('.jum_pes1').on('click',function()
		{
			var mul = $('[name="tahun_mulai_"]').val();
			var smp = $('[name="tahun_sampai_"]').val();
			var id = $(this).attr('id');
			if (id == 0)
			{
				var id = 0;
			}
			var th = $(this).attr('link');
			var jb = $(this).attr('jabatan');
			var ins = $(this).attr('instansi')
			$.ajax({
				type : 'post',
				url : '<?= base_url()?>Kompetensi/edit_pejafung_pk/'+id+'/diklat_fung/jum_pes/jum_dik_fung/'+th,
				data : {jb:jb,ins:ins,mul:mul,smp:smp},
				success : function(data)
				{
					console.log(data);
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Update Jumlah Pejafung');
				}
			})
		})
		$('.jum_dik_tek').on('click',function()
		{
			var mul = $('[name="tahun_mulai_"]').val();
			var smp = $('[name="tahun_sampai_"]').val();
			var id = $(this).attr('id');
			if (id == 0)
			{
				var id = 0;
			}
			var th = $(this).attr('link');
			var jb = $(this).attr('jabatan');
			var ins = $(this).attr('instansi')
			$.ajax({
				type : 'post',
				url : '<?= base_url()?>Kompetensi/edit_pejafung_pk/'+id+'/diklat_tek/jum_dik_tek/jum_pes/'+th,
				data : {jb:jb,ins:ins,mul:mul,smp:smp},
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Update Jumlah Pejafung');
				}
			})
		})
		$('.jum_pes2').on('click',function()
		{
			var mul = $('[name="tahun_mulai_"]').val();
			var smp = $('[name="tahun_sampai_"]').val();
			var id = $(this).attr('id');
			if (id == 0)
			{
				var id = 0;
			}
			var th = $(this).attr('link');
			var jb = $(this).attr('jabatan');
			var ins = $(this).attr('instansi')
			$.ajax({
				type : 'post',
				url : '<?= base_url()?>Kompetensi/edit_pejafung_pk/'+id+'/diklat_tek/jum_pes/jum_dik_tek/'+th,
				data : {jb:jb,ins:ins,mul:mul,smp:smp},
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Update Jumlah Pejafung');
				}
			})
		})
		$('.diklat_lain').on('click',function()
		{
			var mul = $('[name="tahun_mulai_"]').val();
			var smp = $('[name="tahun_sampai_"]').val();
			var id = $(this).attr('id');
			if (id == 0)
			{
				var id = 0;
			}
			var th = $(this).attr('link');
			var jb = $(this).attr('jabatan');
			var ins = $(this).attr('instansi')
			$.ajax({
				type : 'post',
				url : '<?= base_url()?>Kompetensi/edit_pejafung_pk/'+id+'/dik_lain/-/-/'+th,
				data : {jb:jb,ins:ins,mul:mul,smp:smp},
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Update Jumlah Pejafung');
				}
			})
		})
	})
</script>