<form method="post" id="formupdate">
  <input type="hidden" name="id" value="<?=$id?>">
  <div id="editor">
  <?php
    if($act=="update")
    {?>
      <textarea name="forum" class='ckeditor' style="margin-top: 30px;"><?=$forum->isi?></textarea>
    <?php }
    else
    {
      echo '<textarea name="forum" class="ckeditor" style="margin-top: 30px;"></textarea>';
    }
  ?>
  </div>
</form>
<br>
  <button class="btn btn-primary <?=$act?> btn-sm btn-block"><i class="fa fa-save"></i> &nbsp;Save</button>


    <script>
      CKEDITOR.replace( 'forum' );
    </script>
  <script>
    $(document).ready(function()
    {
      $('.update').on('click',function()
      {
        $('#formupdate').attr('action','<?=base_url()?>Forum/edit_forum/');
        $('#formupdate').submit();
      })
      $('.simpan').on('click',function()
      {
        $('#formupdate').attr('action','<?=base_url()?>Forum/simpan_forum/');
        $('#formupdate').submit();
      })
    })
  </script>