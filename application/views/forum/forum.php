<div class="content">
	<form id="editform" method="post">
		<input type="hidden" name="id">
	</form>
	<h3 align="center"><b>Forum Diskusi</b></h3>
	<a href="<?=base_url()?>Forum/tambah_view_forum"><button style="border-radius: 10px;" class="btn btn-success btn-sm"> <i class="fa fa-plus"></i> &nbsp; Post To Forum</button><br><br></a>
	<?php
	if(empty($forum))
	{
		echo '<h3 style="margin-top:7em; font-weight:bold" align="center">~ Forum Diskusi Sedang Kosong ~</h3>';
	}
	foreach ($forum as $key => $fr) {?>
	<div class="panel panel-info">
		<div class="panel-heading">
    <?php
    $hak = $this->session->userdata('hak_akses');
    if ($hak == "admin")
    {
      echo '<div class="pull-right"><i class="fa fa-pencil edit" style="cursor: pointer;" id="'.$fr->id.'"></i>&emsp;<i class="fa fa-remove delete" id="'.$fr->id.'"  style="cursor: pointer;"></i></div>';
    }
    ?>
			<i class="fa fa-circle text-success"></i><i> &nbsp; <b><?=$fr->nama_user?></b></i><br>
			<i><?=$fr->tgl_posting?></i>
		</div>
		<div class="panel-body">
			<p>
				<?=$fr->isi;?>
			</p>
		</div>
	</div>
	<?php }?>
</div>

<script type="text/javascript">
	$(document).ready(function(){
    $('.delete').on('click',function()
    {
      var id = $(this).attr('id');
      swal({
            title: "Apakah Anda Yakin?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              delete_data_forum(id);
            } else {
              swal("Batal Menghapus Data!", {
              icon: "error",
            });
            }
          });
    })
    function delete_data_forum(id)
    {
      $.ajax({
       method : 'get',
       url : '<?=base_url()?>Forum/delete_forum/'+id,
       success : function(data)
       {
          swal({
            title : "Postingan Berhasil Di Hapus !!",
            icon: "success",
          });
          document.location.reload(true);
       }
      })
    }
		$('.edit').on('click',function()
		{
			var id = $(this).attr('id');
      // alert(id);die();
			$('[name="id"]').val(id);
			$('#editform').attr('action','<?=base_url()?>Forum/edit_view_forum/');
			$('#editform').submit();
		})
	})
</script>
