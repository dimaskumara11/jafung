<input type="hidden" class="btn btn-primary edit_alumni_modal" data-toggle="modal" data-target="#exampleModal">
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title">Tambah User</h3>
      </div>
      <div class="modal-body">
        <form id="formtambah" action="#" method="post">
          <div class="form-group">
            <label>NIK</label>
            <input type="text" name="nik" class="form-control" placeholder="Masukkan Nomor Induk Kementrian">
          </div>
          <div class="form-group">
            <label>Nama Kementrian</label>
            <select class="form-control" name="nama_kementrian">
              <option value="0">Pilih Kementrian</option>
              <?php foreach ($instansi as $key => $ins) 
              {?>
                <option value="<?=$ins->id_instansi?>"><?=$ins->nama_instansi?></option>
              <?php } ?>
            </select>
            <input type="hidden" name="id_jabatan" class="form-control" placeholder="Masukkan jabatan">
          </div>
          <div class="form-group">
            <label>Nama pengguna</label>
            <input type="text" name="nama_user" class="form-control" placeholder="Masukkan nama pengguna">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Masukkan nama password" required="">
          </div>
          <div class="form-group">
            <label>Hak Akses</label>
            <select name="hak_akses" class="form-control">
              <option value="0">Pilih Hak Akses</option>
              <option value="admin">Admin</option>
              <option value="kementrian">Kementrian</option>
            </select>
          </div>
          <div class="form-group">
            <label>No HP</label>
            <input type="text" name="no_hp" class="form-control" placeholder="Masukkan Nomor Handphone">
          </div>
          <div class="form-group">
            <label>E-mail</label>
            <input type="text" name="email" class="form-control" placeholder="Masukkan E-mail">
          </div>
          <button type="button" class="btn btn-primary tambah"><i class="glyphicon glyphicon-floppy-save"></i> Simpan</button>
          <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div><div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judulmodal">Tambah User</h3>
      </div>
      <div class="modal-body">
        <form id="formupdate" action="#" method="post">
          <div class="form-group">
            <label>NIK</label>
            <input type="text" name="nik" class="form-control" placeholder="Masukkan Nomor Induk Kementrian">
          </div>
          <div class="form-group">
            <label>Nama Kementrian</label>
            <select class="form-control" name="nama_kementrian">
              <option value="0">Pilih Kementrian</option>
              <?php foreach ($instansi as $key => $ins) 
              {?>
                <option value="<?=$ins->id_instansi?>"><?=$ins->nama_instansi?></option>
              <?php } ?>
            </select>
            <input type="hidden" name="id_user" class="form-control" placeholder="Masukkan jabatan">
          </div>
          <div class="form-group">
            <label>Nama pengguna</label>
            <input type="text" name="nama_user" class="form-control" placeholder="Masukkan nama pengguna">
          </div>
          <div class="form-group">
            <label>Hak Akses</label>
            <select name="hak_akses" class="form-control">
              <option value="0">Pilih Hak Akses</option>
              <option value="admin">Admin</option>
              <option value="kementrian">Kementrian</option>
            </select>
          </div>
          <div class="form-group">
            <label>No HP</label>
            <input type="text" name="no_hp" class="form-control" placeholder="Masukkan Nomor Handphone">
          </div>
          <div class="form-group">
            <label>E-mail</label>
            <input type="text" name="email" class="form-control" placeholder="Masukkan E-mail">
          </div>
          <button type="button" class="btn btn-primary update"><i class="glyphicon glyphicon-floppy-save"></i> Simpan</button>
          <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <input type="hidden" name="password_alumni" value="<?=$this->session->userdata('password')?>">
	<h3 align="center"><b>Daftar User</b></h3>
<button type="button" class="btn btn-primary edit_alumni_modal1" data-toggle="modal" data-target="#exampleModal1"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah User</button><br><br>
	<div class="panel panel-primary">
		<div class="panel panel-body">
			<table id="example1" class="table table-bordered table-striped">
        <thead class="btn-primary">
          <tr>
          	<th>No</th>
            <th>NIK</th>
            <th>Nama Kementrian</th>
            <th>Nama Pengguna</th>
            <th>Hak Akses</th>
            <th>No HP</th>
            <th>E-mail</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody id="show_data">
        </tbody>
      </table>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function()
  {
    tampil_data();
    function tampil_data()
    {
      $.ajax({
      type  : 'POST',
      url   : '<?php echo base_url()?>User/tampil_user',
      dataType : 'json',
      success : function(data)
      {
// console.log(data);
          var html = '';
          var i;
          var j=1;
          for(i=0; i<data.length; i++){
              html += '<tr>'+
                      '<td>'+ j++ +'</td>'+
                      '<td>'+data[i].nik+'</td>'+
                      '<td>'+data[i].nama_instansi+'</td>'+
                      '<td>'+data[i].nama_user+'</td>'+
                      '<td>'+data[i].hak_akses+'</td>'+
                      '<td>'+data[i].no_hp+'</td>'+
                      '<td>'+data[i].email+'</td>'+
                      '<td>'+'<button type="button" class="btn btn-xs btn-warning edit" link="'+data[i].id_user+'"><i class="glyphicon glyphicon-edit"></i></button> '+
                      '<button type="button" class="btn btn-xs btn-danger delete" link="'+data[i].id_user+'"><i class="glyphicon glyphicon-remove"></a></button>'+
                          '</td>'+
                      '</tr>';
          }
          $('#show_data').html(html);
      }
      })
    }
    $('.edit_alumni_modal1').on('click',function()
    {
      $('[name="nama_kementrian"]').val('0');
      $('[name="nama_user"]').val('');
      $('[name="nik"]').val('');
      $('[name="email"]').val('');
      $('[name="no_hp"]').val('');
      $('[name="hak_akses"]').val('0');
    })
    $('#show_data').on('click','.edit',function()
    {
    	var id = $(this).attr('link');
      $.ajax({
       method : 'get',
       url : '<?=base_url()?>User/edit_user/'+id,
       dataType : 'JSON',
       success : function(data)
       {
          $('.judulmodal').html('Edit User')
          $('[name="id_user"]').val(data.id_user);
          $('[name="nama_kementrian"]').val(data.nama_kementrian);
          $('[name="nama_user"]').val(data.nama_user);
          $('[name="nik"]').val(data.nik);
          $('[name="email"]').val(data.email);
          $('[name="no_hp"]').val(data.no_hp);
          $('[name="hak_akses"]').val(data.hak_akses);
          $('.edit_alumni_modal').click();
       }
      })
    })

    $('#show_data').on('click','.delete',function()
    {
      var id = $(this).attr('link');
      swal({
            title: "Apakah Anda Yakin?",
            subtitle:"Ingin menghapus data.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              delete_user(id);
            } else {
              swal("Batal Menghapus Data!", {
              icon: "error",
            });
            }
          });
    })
    function delete_user(id)
    {
      $.ajax({
       method : 'get',
       url : '<?=base_url()?>User/delete_user/'+id,
       success : function(data)
       {
          swal({
            title : "Hapus Data Berhasil !!",
            icon: "success",
          });
          tampil_data();
       }
      })
    }
    $('.update').on('click',function()
    {
      $.ajax({
       method : 'post',
       url : '<?=base_url()?>User/update_user',
       data : $('#formupdate').serialize(),
       success : function(data)
       {
        $('.close').click();
          swal({
            title : "Update Data Berhasil !!",
            icon: "success",
          });
          tampil_data();
       }
      })
    })
    $('.tambah').on('click',function()
    {
      $.ajax({
       method : 'post',
       url : '<?= base_url()?>User/tambah_user',
       data : $('#formtambah').serialize(),
       dataType : 'JSON',
       success : function(data)
       {
        $('.close').click();
          swal({
            title : "Tambah Data Berhasil !!",
            icon: "success",
          });
          tampil_data();
       }
      })
    })
  })
</script>