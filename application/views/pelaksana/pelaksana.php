<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
      	<div class="pull-left"></div>
      	<div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title">Update Jafung</h3>
      </div>
      <div class="modal-body">
    	<form action="<?=base_url()?>Pelaksana/update_jafung_pelaksana" method="post" id="formupdate">
    		<input type="hidden" name="tahun" value="<?=$tahun?>">
    		<input type="hidden" name="idpelaksana" value="">
    	<input type="hidden" name="pendidikan" value="">
    	<input type="hidden" name="usia" value="">
		  <div class="form-group">
		  	<label>Masukkan jumlah</label>
	  		<input type="text" class="form-control" name="jumlah" placeholder="Masukkan Jumlah" required="">
	  	</div>
		  <button type="submit" class="btn btn-primary update btn-sm"><i class="glyphicon glyphicon-ok"></i></button>
		  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></button>
		</form>
      </div>
    </div>
  </div>
</div>
<br>
<div style="width: 300px" class="pull-right">
<form action="<?=base_url()?>Pelaksana/" method="post">
<div class="col-md-8">
	<select name="tahun" class="form-control" required="">
	<?php
	for($i=2018;$i<2031;$i++)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	?>
	</select>
</div>
<div class="col-md-1"><button type="submit" class="btn btn-success"><b>Jelajahi</b></button></div>
</form>
</div>
<br><br>
<div class="panel panel-primary">
	<div class="panel panel-heading">
		<h3 align="center">DAFTAR PELAKSANA BERDASARKAN PENDIDIKAN TAHUN <?=$tahun?></h3>
	</div>
		<div class="panel-body table-responsive">
		<table class="table table-hover table-bordered">
			<thead style="font-weight: bold;text-align: center;">
				<tr class="btn-success">
					<td rowspan="4">No</td>
					<td rowspan="4">Instansi Pembina</td>
					<td rowspan="4">Nama Jabatan</td>
					<td rowspan="4">Urusan</td>
					<td colspan="10">Jumlah Pejabat Pelaksana</td>
					<td rowspan="4" class="btn-warning">Jumlah</td>
				</tr>
				<tr class="btn-success">
					<td colspan="10">Pendidikan</td>
				</tr>
				<tr class="btn-primary">
					<td colspan="2">S3</td>
					<td colspan="2">S2</td>
					<td colspan="2">S1</td>
					<td colspan="2">D3</td>
					<td colspan="2"><= SMA</td>
				</tr>
				<tr class="btn-primary">
					<td>Usia <= 50</td>
					<td>Usia > 50</td>
					<td>Usia <= 50</td>
					<td>Usia > 50</td>
					<td>Usia <= 50</td>
					<td>Usia > 50</td>
					<td>Usia <= 50</td>
					<td>Usia > 50</td>
					<td>Usia <= 50</td>
					<td>Usia > 50</td>
				</tr>
			</thead>
			<tbody>
			<?php
			if(empty($pelaksana))
			{
				echo '<tr><th><center><br>Data Kosong</center></th></tr>';
			} 
			else
			{
				$s3_1_ = 0;
				$s3_2_ = 0;
				$s2_1_ = 0;
				$s2_2_ = 0;
				$s1_1_ = 0;
				$s1_2_ = 0;
				$d3_1_ = 0;
				$d3_2_ = 0;
				$sma_1_ = 0;
				$sma_2_ = 0;
				$total_ = 0;
				$i =1 ; foreach ($pelaksana as $key => $pl) 
				{?>
				<tr align="center">
					<td><?=$i++?></td>
					<td><?=$pl->nama_instansi?></td>
					<td><?=$pl->nama_jabatan?></td>
					<td>
							<form action="<?=base_url()?>Pelaksana/update_jafung_pelaksana1" method="post">
					    	<input type="hidden" name="id_pelaksana" value="<?=$pl->id_pelaksana?>">
								<input type="hidden" name="tahun" value="<?=$tahun?>">
								<textarea class="form-control" name="deskripsi"><?=$pl->urusan?></textarea>
								<button type="submit" class="btn btn-info btn-xs btn-block">Update</button>
							</form>
					</td>
						<?php 
						$s3 = json_decode($pl->pendidikan_s3);
						$s3_1 = intval($s3[0]->usiabawah50);
						$s3_2 = intval($s3[0]->usiaatas50);
						$s3_1_ += $s3_1;
						$s3_2_ += $s3_2;
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiabawah50" id1="pendidikan_s3" class="edit">'.$s3_1.'</td>';
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiaatas50" id1="pendidikan_s3" class="edit">'.$s3_2.'</td>';
						$s2 = json_decode($pl->pendidikan_s2);
						$s2_1 = intval($s2[0]->usiabawah50);
						$s2_2 = intval($s2[0]->usiaatas50);
						$s2_1_ +=$s2_1;
						$s2_2_ +=$s2_2;
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiabawah50" id1="pendidikan_s2" class="edit">'.$s2_1.'</td>';
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiaatas50" id1="pendidikan_s2" class="edit">'.$s2_2.'</td>';
						$s1 = json_decode($pl->pendidikan_s1);
						$s1_1 = intval($s1[0]->usiabawah50);
						$s1_2 = intval($s1[0]->usiaatas50);
						$s1_1_ +=$s1_1;
						$s1_2_ +=$s1_2;
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiabawah50" id1="pendidikan_s1" class="edit">'.$s1_1.'</td>';
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiaatas50" id1="pendidikan_s1" class="edit">'.$s1_2.'</td>';
						$d3 = json_decode($pl->pendidikan_d3);
						$d3_1 = intval($d3[0]->usiabawah50);
						$d3_2 = intval($d3[0]->usiaatas50);
						$d3_1_ +=$d3_1;
						$d3_2_ +=$d3_2;
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiabawah50" id1="pendidikan_d3" class="edit">'.$d3_1.'</td>';
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiaatas50" id1="pendidikan_d3" class="edit">'.$d3_2.'</td>';
						$sma = json_decode($pl->pendidikan_sma);
						$sma_1 = intval($sma[0]->usiabawah50);
						$sma_2 = intval($sma[0]->usiaatas50);
						$sma_1_ +=$sma_1;
						$sma_2_ +=$sma_2;
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiabawah50" id1="pendidikan_sma" class="edit">'.$sma_1.'</td>';
						echo '<td style="cursor:pointer" link="'.$pl->id_pelaksana.'" id="usiaatas50" id1="pendidikan_sma" class="edit">'.$sma_2.'</td>';
						$total = $sma_1+$sma_2+$s3_1+$s3_2+$s2_1+$s2_2+$s1_1+$s1_2+$d3_1+$d3_2;
						$total_ += $total;
						echo '<td align="center" style="font-weight: bold;font-size: 18px"><b>'.$total.'</b></td>';
						 ?>
				</tr>
			<?php }?>
				<tr align="center" style="font-weight: bold;font-size: 18px">
					<td colspan="4">Total</td>
					<td><?=$s3_1_?></td>
					<td><?=$s3_2_?></td>
					<td><?=$s2_1_?></td>
					<td><?=$s2_2_?></td>
					<td><?=$s1_1_?></td>
					<td><?=$s1_2_?></td>
					<td><?=$d3_1_?></td>
					<td><?=$d3_2_?></td>
					<td><?=$sma_1_?></td>
					<td><?=$sma_2_?></td>
					<td><?=$total_?></td>
				</tr>
			<? }?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.edit').on('click',function()
		{
			$('#exampleModal1').modal();
			var id = $(this).attr('link');
			var id1 = $(this).attr('id');
			var id2 = $(this).attr('id1');
			$.ajax({
				type : 'POST',
				url : '<?=base_url()?>Pelaksana/edit_pelaksana/'+id,
				data : {id1:id1,id2:id2},
				success : function(data)
				{
					$('[name="pendidikan"]').val(id2);
					$('[name="usia"]').val(id1);
					$('[name="idpelaksana"]').val(id);
					$('[name="jumlah"]').val(data);
				}
			})
		})
	})
</script>