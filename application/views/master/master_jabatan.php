<input type="hidden" class="btn btn-primary edit_alumni_modal" data-toggle="modal" data-target="#exampleModal">
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title">Tambah Jabatan</h3>
      </div>
      <div class="modal-body">
        <form id="formtambah" action="#" method="post">
          <div class="form-group">
            <label>Nama Jabatan</label>
            <input type="hidden" name="id_jabatan" class="form-control" placeholder="Masukkan jabatan">
            <input type="text" name="nama_jabatan" class="form-control" placeholder="Masukkan jabatan" required="">
          </div>
          <button type="button" class="btn btn-primary tambah"><i class="glyphicon glyphicon-floppy-save"></i> Simpan</button>
          <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div><div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <div class="pull-left"></div>
        <div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judulmodal">Tambah jabatan</h3>
      </div>
      <div class="modal-body">
        <form id="formupdate" action="#" method="post">
          <div class="form-group">
            <label>Nama Jabatan</label>
            <input type="hidden" name="id_jabatan" class="form-control" placeholder="Masukkan jabatan">
            <input type="text" name="nama_jabatan" class="form-control" placeholder="Masukkan jabatan" required="">
          </div>
          <button type="button" class="btn btn-primary update"><i class="glyphicon glyphicon-floppy-save"></i> Simpan</button>
          <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <input type="hidden" name="password_alumni" value="<?=$this->session->userdata('password')?>">
	<h3 align="center"><b>Daftar Jabatan</b></h3>
<button type="button" class="btn btn-primary edit_alumni_modal1" data-toggle="modal" data-target="#exampleModal1"><i class="glyphicon glyphicon-plus"></i> &nbsp; Tambah Jabatan</button><br><br>
	<div class="panel panel-primary">
		<div class="panel panel-body table-responsive">
			<table class="table table-bordered table-striped"  id="example3">
        <thead class="btn-primary">
          <tr>
          	<th width="10px">No</th>
            <th>Nama Jabatan</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody id="show_data">
        </tbody>
      </table>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function()
  {
    tampil_data();
    function tampil_data()
    {
      $.ajax({
      type  : 'POST',
      url   : '<?php echo base_url()?>Master_jabatan/tampil_jabatan',
      dataType : 'json',
      success : function(data)
      {
// console.log(data);
          if(data==0)
          {
            html = '<tr><th colspan="3"><center><br>Data Kosong</center></th></tr>';
          }
          else
          {
            var html = '';
            var i;
            var j=1;
            for(i=0; i<data.length; i++){
                html += '<tr>'+
                        '<td>'+ j++ +'</td>'+
                        '<td>'+data[i].nama_jabatan+'</td>'+
                        '<td width="200px">'+'<button type="button" class="btn btn-xs btn-warning edit" link="'+data[i].id_jabatan+'"><i class="glyphicon glyphicon-pencil"></i> &nbsp;Edit &nbsp;</button>&emsp;'+
                        '<button type="button" class="btn btn-xs btn-danger delete" link="'+data[i].id_jabatan+'"><i class="glyphicon glyphicon-trash"></i> &nbsp;Hapus &nbsp;</button>'+
                            '</td>'+
                        '</tr>';
            }
          }
          $('#show_data').html(html);
      }
      })
    }
    $('.edit_alumni_modal1').on('click',function()
    {
      $('[name="nama_jabatan"]').val('');
    })
    $('#show_data').on('click','.edit',function()
    {
    	var id = $(this).attr('link');
      $.ajax({
       method : 'get',
       url : '<?=base_url()?>Master_jabatan/edit_jabatan/'+id,
       dataType : 'JSON',
       success : function(data)
       {
          $('.judulmodal').html('Edit Jabatan')
          $('[name="id_jabatan"]').val(data.id_jabatan);
          $('[name="nama_jabatan"]').val(data.nama_jabatan);
          $('.edit_alumni_modal').click();
       }
      })
    })

    $('#show_data').on('click','.delete',function()
    {
      var id = $(this).attr('link');
      swal({
            title: "Apakah Anda Yakin?",
            subtitle:"Ingin menghapus data.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              delete_data_jabatan(id);
            } else {
              swal("Batal Menghapus Data!", {
              icon: "error",
            });
            }
          });
    })
    function delete_data_jabatan(id)
    {
      $.ajax({
       method : 'get',
       url : '<?=base_url()?>Master_jabatan/delete_jabatan/'+id,
       success : function(data)
       {
          swal({
            title : "Hapus Data Berhasil !!",
            icon: "success",
          });
          tampil_data();
       }
      })
    }
    $('.update').on('click',function()
    {
      $.ajax({
       method : 'post',
       url : '<?=base_url()?>Master_jabatan/update_jabatan',
       data : $('#formupdate').serialize(),
       success : function(data)
       {
        $('.close').click();
          swal({
            title : "Update Data Berhasil !!",
            icon: "success",
          });
          tampil_data();
       }
      })
    })
    $('.tambah').on('click',function()
    {
      if ($('[name="nama_jabatan"]').val() == 0)
      {          
        swal({
            title : "Harap Isi Nama Jabatan !!",
            icon: "error",
          });die();
      }
      $.ajax({
       method : 'post',
       url : '<?= base_url()?>Master_jabatan/tambah_jabatan',
       data : $('#formtambah').serialize(),
       dataType : 'JSON',
       success : function(data)
       {
        $('.close').click();
          swal({
            title : "Tambah Data Berhasil !!",
            icon: "success",
          });
          tampil_data();
       }
      })
    })
  })
</script>