<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Jafung</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <link href="<?=base_url()?>template/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>template/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>template/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>template/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="<?=base_url()?>highchart/code/highcharts.js"></script>
    <script src="<?=base_url()?>highchart/code/modules/exporting.js"></script>
    <script src="<?=base_url()?>highchart/code/modules/export-data.js"></script>
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      <?php
  $info = $this->session->flashdata('alert');
    if(!empty($info)) 
    {
      echo $info;
    }
  ?>
  
      <header class="main-header">
        <!-- Logo -->
        <a href="<?=base_url()?>" class="logo"><b>Jafung</b></a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=base_url()?>template/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"> <?=$this->session->userdata('nama')?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=base_url()?>template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    <p>
                      <?=$this->session->userdata('nama')?>
                      <small>E-mail : <?=$this->session->userdata('email')?></small>
                    </p>
                  </li>
                  <li class="user-footer">
                    <a href="<?=base_url()?>Admin/logout" class="btn btn-default btn-block">Sign out</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?=$this->session->userdata('nama')?></p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="<?=base_url()?>Admin">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <?php 
            if ($this->session->userdata('hak_akses')!="admin")
            {?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-plus"></i> <span>Mater Data</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>Master_relasi_fungsional"><i class="fa fa-circle-o"></i> Master Relasi Fungsional</a></li>
                <li><a href="<?=base_url()?>Master_relasi_pelaksana"><i class="fa fa-circle-o"></i> Master Relasi Pelaksana</a></li>
              </ul>
            </li>
            <?php }?>
            <?php 
            if ($this->session->userdata('hak_akses')=="admin")
            {?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-plus"></i> <span>Mater Data</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>Master_instansi"><i class="fa fa-circle-o"></i> Master Instansi</a></li>
                <li><a href="<?=base_url()?>Master_jabatan"><i class="fa fa-circle-o"></i> Master Jabatan</a></li>
                <li><a href="<?=base_url()?>Master_relasi_fungsional"><i class="fa fa-circle-o"></i> Master Relasi Fungsional</a></li>
                <li><a href="<?=base_url()?>Master_relasi_pelaksana"><i class="fa fa-circle-o"></i> Master Relasi Pelaksana</a></li>
              </ul>
            </li>
          <?php }?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file"></i> <span>Fungsional</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
              <a href="<?=base_url()?>Pejafung/">
                <i class="fa fa-book"></i> <span>Pejabat Fungsional Aktif</span>
              </a></li>
                <li>
              <a href="<?=base_url()?>Proyeksi/">
                <i class="fa fa-book"></i> <span>Proyeksi Pejabat Fungsional</span>
              </a></li>
                <li>
              <a href="<?=base_url()?>Peraturan_pelaksanaan/">
                <i class="fa fa-book"></i> <span>Peraturan JF</span>
              </a></li>
                <li>
              <a href="<?=base_url()?>Kompetensi">
                <i class="fa fa-book"></i> <span>Kompetensi Teknis JF</span>
              </a></li>
                <li>
              <a href="<?=base_url()?>Profil_jf">
                <i class="fa fa-book"></i> <span>Profil JF</span>
              </a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file"></i> <span>Pelaksana</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?=base_url()?>Pelaksana">
                    <i class="fa fa-book"></i> <span>Pelaksana</span>
                  </a>
                </li>
                <li>
                  <a href="<?=base_url()?>Profil_pelaksana">
                    <i class="fa fa-book"></i> <span>Profil Jabatan Pelaksana</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?=base_url()?>Admin/struktural">
                <i class="fa fa-file"></i> <span>Struktural</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url()?>Admin/informasi">
                <i class="fa fa-file"></i> <span>Informasi</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url()?>Admin/monitoring">
                <i class="fa fa-file"></i> <span>Monitoring</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url()?>Admin/layanan">
                <i class="fa fa-file"></i> <span>Layanan Internal</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?=base_url()?>Admin/operator">
                <i class="fa fa-file"></i> <span>Operator</span>
              </a>
            </li>
            <li class="header">LABELS</li>
            <li><a href="<?=base_url()?>Forum"><i class="fa fa-comments"></i> Forum Diskusi</a></li>
            
            <?php 
            if ($this->session->userdata('hak_akses')=="admin")
            {?>
            <li><a href="<?=base_url()?>User"><i class="fa fa-cog"></i> Registrasi User</a></li>
          <?php }?>
            <li><a href="<?=base_url()?>Profil"><i class="fa fa-user"></i> Profil</a></li>
            <li><a href="<?=base_url()?>Admin/logout"><i class="glyphicon glyphicon-off"></i> Keluar</a></li>
          </ul>
        </section>
      </aside>
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            <?=$hal1 ?>
          </h1>
          <ol class="breadcrumb">
            <?=$hal2?>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php $this->load->view($page);?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.0
        </div>
        <strong>Jafung.</strong> All rights reserved.
      </footer>

    </div><!-- ./wrapper -->

    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <!-- jQuery 2.1.3 -->
    <script src="<?=base_url()?>template/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?=base_url()?>template/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?=base_url()?>template/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>template/dist/js/app.min.js" type="text/javascript"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="<?=base_url()?>template/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?=base_url()?>template/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable(
        {
          "bPaginate": false,
          "processing": true,
        });
        $('#example2').dataTable(
        {
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
  </body>
</html>