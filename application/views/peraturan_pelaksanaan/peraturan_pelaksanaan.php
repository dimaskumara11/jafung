<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
      	<div class="pull-left"></div>
      	<div class="pull-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></div>
        <h3 align="center" class="modal-title judulmodal"></h3>
      </div>
      <div class="modal-body">
      	<div class="edit_pp"></div>
      </div>
    </div>
  </div>
</div>

<div style="width: 300px" class="pull-right">
<form action="<?=base_url()?>Peraturan_pelaksanaan/" method="post">
<div class="col-md-8">
	<select name="tahun" class="form-control" required="">
	<?php
	for($i=2018;$i<2031;$i++)
	{
		echo '<option value="'.$i.'">'.$i.'</option>';
	}
	?>
	</select>
</div>
<div class="col-md-1"><button type="submit" class="btn btn-success"><b>Jelajahi</b></button></div>
</form>
<input type="hidden" name="tahun__" value="<?=$tahun?>">
</div>
<br>
<br>
<div class="content">
	<!-- <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal1"><b><i class="fa fa-plus"></i> Tambah Tahun</b></button><br><br> -->
<div class="panel panel-primary">
	<div class="panel panel-heading">
		<h3 align="center">DAFTAR PERATURAN PELAKSANAAN YANG TELAH DITETAPKAN DALAM RANGKA PEMBINAAN JABATAN FUNGSIONAL TAHUN <?=$tahun?></h3> 
	</div>
	<div class="panel-body table-responsive">
		<table class="table table-hover table-bordered">
			<thead style="font-weight: bold;text-align: center;">
				<tr class="btn-success">
					<td rowspan="4">No</td>
					<td rowspan="4">Nama Jabatan</td>
					<td rowspan="4">Instansi Pembina</td>
					<td colspan="18">Jenis Peraturan</td>
					<td rowspan="4" class="btn-warning">Jumlah</td>
				</tr>
				<tr class="btn-primary">
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
					<td>5</td>
					<td>6</td>
					<td>7</td>
					<td>8</td>
					<td>9</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
					<td>13</td>
					<td>14</td>
					<td>15</td>
					<td>16</td>
					<td>17</td>
					<td>18</td>
				</tr>
				<tr class="btn-primary">
					<td>Petunjuk Teknis Pelaksanaan</td>
					<td>Penyusunan Pedoman Formasi</td>
					<td>Standar Kompetensi Jabatan Fungsional</td>
					<td>Tunjangan Fungsional / Tunjangan Kinerja</td>
					<td>Penetapan Organisasi Profesi Dan Pengukuhan Pengurus Organisasi Profesi</td>
					<td>Kode Etik Profesi Dan Kode Perilaku Jabatan Fungsional</td>
					<td>Standar Kualitas Kerja Jabatan Fungsional</td>
					<td>Petunjuk Teknis Penilaian Angka Kredit</td>
					<td>Tata Kerja Jabatan Fungsional</td>
					<td>Sistem Informasi Dan Evaluasi Jabatan Fungsional</td>
					<td>Pedoman Penyusunan Karya Tulis</td>
					<td>Diklat Jabatan Fungsional</td>
					<td>Batas Usia Pensiun Jabatan Fungsional</td>
					<td>Kualifikasi Pendidikan Jabatan Fungsional</td>
					<td>Mekanisme Sertifikat JF</td>
					<td>Pedoman Diklat / 	Pengembangan Kompetensi JF</td>
					<td>Pedoman Uji Kompetensi</td>
					<td>Pedoman Penilaian Kinerja JF</td>
				</tr>
			</thead>
			<tbody>
			<?php
			// print_r($peraturan);
			if(empty($peraturan))
			{
				echo '<tr><th colspan="2"><center><br>Data Kosong</center></th></tr>';
			} 
			else
			{
				$i = 1;
				foreach ($peraturan as $key => $pp) 
					{?>
						<tr>
							<td><?=$i++?></td>
							<td><?=$pp->nama_jabatan?></td>
							<td><?=$pp->nama_instansi?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="pet_tek_pel" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->pet_tek_pel==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="ped_peny_for" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->ped_peny_for==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="stand_komp_jab_fung" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->stand_komp_jab_fung==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="tun_fung" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->tun_fung==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="pen_or" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->pen_or==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="kod_et_prof" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->kod_et_prof==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="stand_kual_ker" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->stand_kual_ker==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="pet_tek_pen" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->pet_tek_pen==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="tat_ker_jab" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->tat_ker_jab==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="sisfo_ev" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->sisfo_ev==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="ped_peny_kar" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->ped_peny_kar==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="dik_jab_fung" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->dik_jab_fung==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="bat_us_pen" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->bat_us_pen==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="kual_pend" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->kual_pend==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="mek_sert" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->mek_sert==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="ped_dik" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->ped_dik==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="ped_uji" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->ped_uji==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td align="center" style="cursor: pointer" id="<?=$pp->id_peraturan?>" class="ped_pen_kin" data-toggle="modal" data-target="#exampleModal">
							<?php
								if($pp->ped_pen_kin==null)
								{?>
									<i class="fa fa-remove text-red"></i>
								<?php }
								else
								{?>
									<i class="fa fa-check text-blue"></i>
								<?php }
							?></td>
							<td></td>
						</tr>
				 <?php } } ?>
			</tbody>
		</table>
	</div>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.pet_tek_pel').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/pet_tek_pel',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Petunjuk Teknis Pelaksanaan');
				}
			})
		})
		$('.ped_peny_for').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/ped_peny_for',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Pedoman Penyusunan Formasi');
				}
			})
		})
		$('.stand_komp_jab_fung').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/stand_komp_jab_fung',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Standar Kompetensi Jabatan Fungsional');
				}
			})
		})
		$('.tun_fung').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/tun_fung',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Tunjangan Fungsional / Tunjangan Kinerja');
				}
			})
		})
		$('.pen_or').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/pen_or',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Penetapan Organisasi Profesi Dan Pengukuhan Pengurus Organisasi Profesi');
				}
			})
		})
		$('.kod_et_prof').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/kod_et_prof',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Kode Etik Profesi Dan Kode Perilaku Jabatan Fungsional');
				}
			})
		})
		$('.stand_kual_ker').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/stand_kual_ker',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Standar Kualitas Kerja Jabatan Fungsional');
				}
			})
		})
		$('.pet_tek_pen').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/pet_tek_pen',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Petunjuk Teknis Penilaian Angka Kredit');
				}
			})
		})
		$('.tat_ker_jab').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/tat_ker_jab',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Tata Kerja Jabatan Fungsional');
				}
			})
		})
		$('.sisfo_ev').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/sisfo_ev',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Sistem Informasi Dan Evaluasi Jabatan Fungsional');
				}
			})
		})
		$('.ped_peny_kar').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/ped_peny_kar',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Pedoman Penyusunan Karya Tulis');
				}
			})
		})
		$('.dik_jab_fung').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/dik_jab_fung',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Diklat Jabatan Fungsional');
				}
			})
		})
		$('.bat_us_pen').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/bat_us_pen',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Batas Pensiun Jabatan Fungsional');
				}
			})
		})
		$('.kual_pend').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/kual_pend',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Kualifikasi Pendidikan Jabatan Fungsional');
				}
			})
		})
		$('.mek_sert').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/mek_sert',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Mekanisme Sertifikat JF');
				}
			})
		})
		$('.ped_dik').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/ped_dik',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Pedoman Diklat / Pengembangan Kompetensi JF');
				}
			})
		})
		$('.ped_uji').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/ped_uji',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Pedoman Uji Kompetensi');
				}
			})
		})
		$('.ped_pen_kin').on('click',function()
		{
			var id = $(this).attr('id');
			var tahun = $('[name="tahun__"]').val();
			$.ajax({
				url : '<?= base_url()?>Peraturan_pelaksanaan/edit_pejafung_pp/'+id+'/'+tahun+'/ped_pen_kin',
				success : function(data)
				{
					$('.edit_pp').html(data);
					$('.judulmodal').html('Form Upload Dokumen Pedoman Penilaian Kinerja JF');
				}
			})
		})
		$('.edit_pp').on('click','.remove1',function()
		{
			// alert('data');
			var a = $(this).attr('id');
			$('[name="'+a+'"]').val('');
		})
	})
</script>