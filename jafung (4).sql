-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 26 Feb 2019 pada 04.24
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jafung`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `forum`
--

CREATE TABLE IF NOT EXISTS `forum` (
`id` int(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tgl_posting` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `instansi`
--

CREATE TABLE IF NOT EXISTS `instansi` (
`id_instansi` int(11) NOT NULL,
  `nama_instansi` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `instansi`
--

INSERT INTO `instansi` (`id_instansi`, `nama_instansi`) VALUES
(15, 'a'),
(16, 'b'),
(17, 'c'),
(18, 'd'),
(19, 'e'),
(20, 'f'),
(21, 'g'),
(22, 'h'),
(23, 'i'),
(24, 'j'),
(25, 'k'),
(26, 'l');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
`id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `id_instansi` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`, `id_instansi`) VALUES
(2, 'penata ruang', 2),
(3, 'prediksi', 2),
(4, 'penulis', 2),
(5, 'pengisi acara', 2),
(6, 'Pendengar', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pejafung`
--

CREATE TABLE IF NOT EXISTS `pejafung` (
`id_pejafung` int(11) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `no_permai` int(11) NOT NULL,
  `ahli_utama` varchar(255) NOT NULL DEFAULT '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]',
  `ahli_madya` varchar(255) NOT NULL DEFAULT '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]',
  `ahli_muda` varchar(255) NOT NULL DEFAULT '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]',
  `ahli_pertama` varchar(255) NOT NULL DEFAULT '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]',
  `penyelia` varchar(255) NOT NULL DEFAULT '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]',
  `pelaksana_lanjutan` varchar(255) NOT NULL DEFAULT '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]',
  `terampil` varchar(255) NOT NULL DEFAULT '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]',
  `pelaksana_pemula` varchar(255) NOT NULL DEFAULT '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]',
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pejafung`
--

INSERT INTO `pejafung` (`id_pejafung`, `id_instansi`, `id_jabatan`, `no_permai`, `ahli_utama`, `ahli_madya`, `ahli_muda`, `ahli_pertama`, `penyelia`, `pelaksana_lanjutan`, `terampil`, `pelaksana_pemula`, `tahun`) VALUES
(11, 15, 2, 0, '[{"pusatatas50":"1","pusatbawah50":"","provatas50":"","provbawah50":"","kabatas50":"","kabbawah50":""}]', '[{"pusatatas50":"32","pusatbawah50":"","provatas50":"","provbawah50":"","kabatas50":"","kabbawah50":""}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2018),
(12, 15, 3, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2018),
(13, 15, 4, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2018),
(14, 15, 5, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2018),
(15, 15, 2, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2019),
(16, 15, 2, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2016),
(17, 15, 0, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2016),
(18, 16, 2, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2018),
(19, 16, 3, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2018),
(20, 16, 5, 0, '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', '[{"pusatatas50":"0","pusatbawah50":"0","provatas50":"0","provbawah50":"0""kabatas50":"0","kabbawah50":"0"}]', 2018);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelaksana`
--

CREATE TABLE IF NOT EXISTS `pelaksana` (
`id_pelaksana` int(100) NOT NULL,
  `id_instansi` int(100) NOT NULL,
  `id_jabatan` int(100) NOT NULL,
  `urusan` varchar(255) NOT NULL,
  `pendidikan_s3` varchar(255) NOT NULL DEFAULT '[{"usiaatas50":"0","usiabawah50":"0"}]',
  `pendidikan_s2` varchar(255) NOT NULL DEFAULT '	[{"usiaatas50":"0","usiabawah50":"0"}]	',
  `pendidikan_s1` varchar(255) NOT NULL DEFAULT '	[{"usiaatas50":"0","usiabawah50":"0"}]	',
  `pendidikan_d3` varchar(255) NOT NULL DEFAULT '	[{"usiaatas50":"0","usiabawah50":"0"}]	',
  `pendidikan_sma` varchar(255) NOT NULL DEFAULT '	[{"usiaatas50":"0","usiabawah50":"0"}]	',
  `tahun` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pelaksana`
--

INSERT INTO `pelaksana` (`id_pelaksana`, `id_instansi`, `id_jabatan`, `urusan`, `pendidikan_s3`, `pendidikan_s2`, `pendidikan_s1`, `pendidikan_d3`, `pendidikan_sma`, `tahun`) VALUES
(1, 15, 2, 'tes', '[{"usiaatas50":"0","usiabawah50":"2"}]', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', 2019),
(2, 15, 4, 'ts', '[{"usiaatas50":"0","usiabawah50":"0"}]', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', 2019),
(3, 16, 4, 'tes', '[{"usiaatas50":"0","usiabawah50":"0"}]', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', 2018),
(4, 16, 2, 's', '[{"usiaatas50":"0","usiabawah50":"10"}]', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', '	[{"usiaatas50":"0","usiabawah50":"0"}]	', 2018);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembangan_kompetensi`
--

CREATE TABLE IF NOT EXISTS `pengembangan_kompetensi` (
`id_pengembangan` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `komp_tek` text NOT NULL,
  `diklat_fung` varchar(255) NOT NULL DEFAULT '[{"jum_dik_fung":"0","jum_pes":"0"}]',
  `diklat_tek` varchar(255) NOT NULL DEFAULT '[{"jum_dik_tek":"0","jum_pes":"0"}]',
  `dik_lain` varchar(255) NOT NULL DEFAULT '0',
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengembangan_kompetensi`
--

INSERT INTO `pengembangan_kompetensi` (`id_pengembangan`, `id_jabatan`, `id_instansi`, `komp_tek`, `diklat_fung`, `diklat_tek`, `dik_lain`, `tahun`) VALUES
(11, 2, 15, 'tes_dimas_putri_1234\r\n56789', '[{"jum_dik_fung":"232","jum_pes":"534"}]', '[{"jum_dik_tek":"2","jum_pes":"0"}]', '10', 2018),
(12, 3, 15, '', '[{"jum_dik_fung":"2","jum_pes":"0"}]', '[{"jum_dik_tek":"0","jum_pes":"0"}]', '0', 2018),
(13, 4, 15, '', '[{"jum_dik_fung":"0","jum_pes":"0"}]', '[{"jum_dik_tek":"0","jum_pes":"0"}]', '0', 2018),
(14, 5, 15, '', '[{"jum_dik_fung":"0","jum_pes":"0"}]', '[{"jum_dik_tek":"0","jum_pes":"0"}]', '0', 2018),
(17, 2, 15, 'tes', '[{"jum_pes":"2","jum_dik_fung":"1"}]', '[{"jum_dik_tek":"4","jum_pes":"0"}]', '90', 2019),
(18, 2, 15, '', '[{"jum_dik_fung":"0","jum_pes":"0"}]', '[{"jum_dik_tek":"0","jum_pes":"0"}]', '0', 2016),
(19, 0, 15, '', '[{"jum_dik_fung":"0","jum_pes":"0"}]', '[{"jum_dik_tek":"0","jum_pes":"0"}]', '0', 2016),
(20, 2, 16, '', '[{"jum_dik_fung":"0","jum_pes":"0"}]', '[{"jum_dik_tek":"0","jum_pes":"0"}]', '0', 2018),
(21, 3, 16, '', '[{"jum_dik_fung":"0","jum_pes":"0"}]', '[{"jum_dik_tek":"0","jum_pes":"0"}]', '0', 2018),
(22, 5, 16, '', '[{"jum_dik_fung":"0","jum_pes":"0"}]', '[{"jum_dik_tek":"0","jum_pes":"0"}]', '0', 2018);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peraturan_pelaksanaan`
--

CREATE TABLE IF NOT EXISTS `peraturan_pelaksanaan` (
`id_peraturan` int(11) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `tahun` int(5) NOT NULL,
  `pet_tek_pel` varchar(255) NOT NULL,
  `ped_peny_for` varchar(255) NOT NULL,
  `stand_komp_jab_fung` varchar(255) NOT NULL,
  `tun_fung` varchar(255) NOT NULL,
  `pen_or` varchar(255) NOT NULL,
  `kod_et_prof` varchar(255) NOT NULL,
  `stand_kual_ker` varchar(255) NOT NULL,
  `pet_tek_pen` varchar(255) NOT NULL,
  `tat_ker_jab` varchar(255) NOT NULL,
  `sisfo_ev` varchar(255) NOT NULL,
  `ped_peny_kar` varchar(255) NOT NULL,
  `dik_jab_fung` varchar(255) NOT NULL,
  `bat_us_pen` varchar(255) NOT NULL,
  `kual_pend` varchar(255) NOT NULL,
  `mek_sert` varchar(255) NOT NULL,
  `ped_dik` varchar(255) NOT NULL,
  `ped_uji` varchar(255) NOT NULL,
  `ped_pen_kin` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peraturan_pelaksanaan`
--

INSERT INTO `peraturan_pelaksanaan` (`id_peraturan`, `id_instansi`, `id_jabatan`, `tahun`, `pet_tek_pel`, `ped_peny_for`, `stand_komp_jab_fung`, `tun_fung`, `pen_or`, `kod_et_prof`, `stand_kual_ker`, `pet_tek_pen`, `tat_ker_jab`, `sisfo_ev`, `ped_peny_kar`, `dik_jab_fung`, `bat_us_pen`, `kual_pend`, `mek_sert`, `ped_dik`, `ped_uji`, `ped_pen_kin`) VALUES
(11, 15, 2, 2018, '[{"nama_dok":"tes2"},{"dok":"Buku_Report_Tracer_Study_ITB_20142.pdf"},{"dok":"Buku_Report_Tracer_Study_ITB_2014_(1)2.pdf"}]', '[{"nama_dok":"tes"},{"dok":"flow_modul__elearning4.docx"}]', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 15, 3, 2018, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13, 15, 4, 2018, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14, 15, 5, 2018, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15, 15, 2, 2019, '[{"nama_dok":"tes","nomor":"1234","tahun":"2019"},{"dok":"form_tracer_study.xlsx"}]', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(16, 15, 2, 2016, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17, 15, 0, 2016, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18, 16, 2, 2018, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19, 16, 3, 2018, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(20, 16, 5, 2018, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil_jf`
--

CREATE TABLE IF NOT EXISTS `profil_jf` (
`id_profil_jf` int(11) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `kategori` text NOT NULL,
  `kedudukan` text NOT NULL,
  `kompetensi_teknis` text NOT NULL,
  `jenjang_jabatan` text NOT NULL,
  `tahun` varchar(255) NOT NULL,
  `permenpanrb` varchar(255) NOT NULL,
  `junlak_junkis` varchar(255) NOT NULL,
  `pengertian` varchar(255) NOT NULL,
  `tugas_pokok` varchar(255) NOT NULL,
  `perpres_tunjangan` varchar(255) NOT NULL,
  `peraturan_bup` varchar(255) NOT NULL,
  `pengangkatan_pertama` varchar(255) NOT NULL,
  `pengangkatan_penyesuaian` varchar(255) NOT NULL,
  `perpindahan_jabatan_lain` varchar(255) NOT NULL,
  `promosi` varchar(255) NOT NULL,
  `penilaian_kinerja` varchar(255) NOT NULL,
  `pemberhentian` varchar(255) NOT NULL,
  `pengangkatan_kembali` varchar(255) NOT NULL,
  `jenjang_angka_kredit_tunjangan_fungsional` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil_jf`
--

INSERT INTO `profil_jf` (`id_profil_jf`, `id_instansi`, `id_jabatan`, `kategori`, `kedudukan`, `kompetensi_teknis`, `jenjang_jabatan`, `tahun`, `permenpanrb`, `junlak_junkis`, `pengertian`, `tugas_pokok`, `perpres_tunjangan`, `peraturan_bup`, `pengangkatan_pertama`, `pengangkatan_penyesuaian`, `perpindahan_jabatan_lain`, `promosi`, `penilaian_kinerja`, `pemberhentian`, `pengangkatan_kembali`, `jenjang_angka_kredit_tunjangan_fungsional`) VALUES
(2, 15, 2, '["keahlian","keterampilan"]', 'Pusat', 'tes_dimas_putri_1234\r\n56789', '[{"keterampilan":"[\\"Penyelia\\",\\"Pelaksana Lanjutan\\",\\"Pelaksana\\"]"},{"keahlian":"[\\"Ahli Utama\\"]"}]', '2018', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 15, 3, '', '', '', '', '2018', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 15, 4, '', '', '', '', '2018', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, 15, 5, '', '', '', '', '2018', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 15, 2, '["keahlian"]', '["Pusat","Daerah","Pemerintah"]', 'tes', '[{"keterampilan":"[\\"Penyelia\\"]"},{"keahlian":"[\\"Ahli Utama\\"]"}]', '2019', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 15, 2, '', '', '', '', '2016', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 15, 0, '', '', '', '', '2016', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 16, 2, '', '', '', '', '2018', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10, 16, 3, '', '', '', '', '2018', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11, 16, 5, '', '', '', '', '2018', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil_pelaksana`
--

CREATE TABLE IF NOT EXISTS `profil_pelaksana` (
`id_profil_pelaksana` int(11) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `urusan` text NOT NULL,
  `tug_jab` text NOT NULL,
  `urai_tug` text NOT NULL,
  `kual_pend` text NOT NULL,
  `komp_tek` text NOT NULL,
  `syarat_jab` text NOT NULL,
  `tahun` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil_pelaksana`
--

INSERT INTO `profil_pelaksana` (`id_profil_pelaksana`, `id_instansi`, `id_jabatan`, `urusan`, `tug_jab`, `urai_tug`, `kual_pend`, `komp_tek`, `syarat_jab`, `tahun`) VALUES
(1, 15, 2, 'tes_', 'tes_1234', '', '', '', '', 2019),
(2, 15, 4, 'ts', '', '', '', '', '', 2019),
(3, 16, 4, 'tes', '', '', '', '', '', 2018),
(4, 16, 2, 'tugas', 'tugas1', '', '', '', '', 2018);

-- --------------------------------------------------------

--
-- Struktur dari tabel `proyeksi`
--

CREATE TABLE IF NOT EXISTS `proyeksi` (
`id_pejafung` int(11) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `no_permai` int(11) NOT NULL,
  `ahli_utama` int(255) NOT NULL,
  `ahli_madya` int(255) NOT NULL,
  `ahli_muda` int(255) NOT NULL,
  `ahli_pertama` int(255) NOT NULL,
  `penyelia` int(255) NOT NULL,
  `pelaksana_lanjutan` int(255) NOT NULL,
  `terampil` int(255) NOT NULL,
  `pelaksana_pemula` int(255) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `proyeksi`
--

INSERT INTO `proyeksi` (`id_pejafung`, `id_instansi`, `id_jabatan`, `no_permai`, `ahli_utama`, `ahli_madya`, `ahli_muda`, `ahli_pertama`, `penyelia`, `pelaksana_lanjutan`, `terampil`, `pelaksana_pemula`, `tahun`) VALUES
(11, 15, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2018),
(12, 15, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2018),
(13, 15, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2018),
(14, 15, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2018),
(15, 15, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2019),
(16, 15, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2016),
(17, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2016),
(18, 16, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2018),
(19, 16, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2018),
(20, 16, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2018);

-- --------------------------------------------------------

--
-- Struktur dari tabel `relasi`
--

CREATE TABLE IF NOT EXISTS `relasi` (
`id_relasi` int(11) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `tahun` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `relasi`
--

INSERT INTO `relasi` (`id_relasi`, `id_instansi`, `id_jabatan`, `tahun`) VALUES
(11, 15, 2, 2018),
(12, 15, 3, 2018),
(13, 15, 4, 2018),
(14, 15, 5, 2018),
(15, 15, 2, 2019),
(16, 15, 2, 2016),
(17, 15, 0, 2016),
(18, 16, 2, 2018),
(19, 16, 3, 2018),
(20, 16, 5, 2018);

-- --------------------------------------------------------

--
-- Struktur dari tabel `relasi_pelaksana`
--

CREATE TABLE IF NOT EXISTS `relasi_pelaksana` (
`id_relasi` int(11) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `tahun` int(6) NOT NULL,
  `urusan` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `relasi_pelaksana`
--

INSERT INTO `relasi_pelaksana` (`id_relasi`, `id_instansi`, `id_jabatan`, `tahun`, `urusan`) VALUES
(1, 15, 2, 2019, 'tes'),
(2, 15, 4, 2019, 'ts'),
(3, 16, 4, 2018, 'tes'),
(4, 16, 2, 2018, 'ts');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id_user` int(11) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `nama_kementrian` int(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `hak_akses` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nik`, `nama_user`, `nama_kementrian`, `password`, `no_hp`, `email`, `hak_akses`) VALUES
(1, 'KM1112', 'SUGITO', 15, 'f02051195f7722392b06267b137d5f03', '0878705229622222', 'sugito@gmail.com1', 'kementrian'),
(2, 'KM112', 'ARIS SUMANTO', 2, '635c818fce47c3ba569d7a4e6acf4cc9', '', '', 'admin'),
(3, 'KM1113', 'Tedy', 16, 'cc1e1887fb2c20cccc72a729c73fb73b', '07784523220', 'dhiemazoey1234@gmail.com', 'kementrian');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forum`
--
ALTER TABLE `forum`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instansi`
--
ALTER TABLE `instansi`
 ADD PRIMARY KEY (`id_instansi`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
 ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `pejafung`
--
ALTER TABLE `pejafung`
 ADD PRIMARY KEY (`id_pejafung`);

--
-- Indexes for table `pelaksana`
--
ALTER TABLE `pelaksana`
 ADD PRIMARY KEY (`id_pelaksana`);

--
-- Indexes for table `pengembangan_kompetensi`
--
ALTER TABLE `pengembangan_kompetensi`
 ADD PRIMARY KEY (`id_pengembangan`);

--
-- Indexes for table `peraturan_pelaksanaan`
--
ALTER TABLE `peraturan_pelaksanaan`
 ADD PRIMARY KEY (`id_peraturan`);

--
-- Indexes for table `profil_jf`
--
ALTER TABLE `profil_jf`
 ADD PRIMARY KEY (`id_profil_jf`);

--
-- Indexes for table `profil_pelaksana`
--
ALTER TABLE `profil_pelaksana`
 ADD PRIMARY KEY (`id_profil_pelaksana`);

--
-- Indexes for table `proyeksi`
--
ALTER TABLE `proyeksi`
 ADD PRIMARY KEY (`id_pejafung`);

--
-- Indexes for table `relasi`
--
ALTER TABLE `relasi`
 ADD PRIMARY KEY (`id_relasi`);

--
-- Indexes for table `relasi_pelaksana`
--
ALTER TABLE `relasi_pelaksana`
 ADD PRIMARY KEY (`id_relasi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forum`
--
ALTER TABLE `forum`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `instansi`
--
ALTER TABLE `instansi`
MODIFY `id_instansi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pejafung`
--
ALTER TABLE `pejafung`
MODIFY `id_pejafung` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pelaksana`
--
ALTER TABLE `pelaksana`
MODIFY `id_pelaksana` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pengembangan_kompetensi`
--
ALTER TABLE `pengembangan_kompetensi`
MODIFY `id_pengembangan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `peraturan_pelaksanaan`
--
ALTER TABLE `peraturan_pelaksanaan`
MODIFY `id_peraturan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `profil_jf`
--
ALTER TABLE `profil_jf`
MODIFY `id_profil_jf` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profil_pelaksana`
--
ALTER TABLE `profil_pelaksana`
MODIFY `id_profil_pelaksana` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `proyeksi`
--
ALTER TABLE `proyeksi`
MODIFY `id_pejafung` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `relasi`
--
ALTER TABLE `relasi`
MODIFY `id_relasi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `relasi_pelaksana`
--
ALTER TABLE `relasi_pelaksana`
MODIFY `id_relasi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
